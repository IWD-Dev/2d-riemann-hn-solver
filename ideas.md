# Improvements

## TODO

### Matrix Grid Dividing
- to speedup calculation, divide into active and nonactive cell groups (additionally to dry cell skip)

### Live Visualization
- use vistools to provide live rendering

### extend BC scheme to in-grid cells
- have eta, discharge inside calculation grid

### optional bloating of grid dimension
- may improve results
- an easy way to make problems bigger for better OpenMP Usage
- initialize via interpolation: linear, hermite, spline, ... 

## DONE

### state buffer
- for better handling of unforseen drastic changes in simulation
- shall be used to improve variable time stepping
