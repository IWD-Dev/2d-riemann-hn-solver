#include "stdafx.h"

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "io/io_util.h"

namespace io {
namespace util {

	std::string read_file_as_text(const std::string &path)
	{
		std::ifstream file(path);
		if (!file.is_open()) {
			std::cerr << "could not find given path: " << path << std::endl;
			return "";
		}
		file.seekg(0, std::ifstream::end);
		size_t length = file.tellg();
		std::vector<char> content;
		content.resize(length);
		file.seekg(0, std::ifstream::beg);
		file.read(content.data(), length);
		return std::string(content.data(), length);
	}

	// simple implementation, but fails if file is blocked or one has no read rights
	bool file_exists(const std::string &path) { return std::ifstream(path).good(); }

	std::string remove_extension(const std::string &filename)
	{
		size_t lastdot = filename.find_last_of(".");
		if (lastdot == std::string::npos) return filename;
		return filename.substr(0, lastdot);
	}
}
}
