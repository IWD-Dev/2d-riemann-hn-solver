#pragma once

#include <string>

namespace io {
namespace util {
	std::string read_file_as_text(const std::string &path);
	bool file_exists(const std::string &path);
	std::string remove_extension(const std::string &filename);
}
}
