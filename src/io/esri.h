#pragma once

// https://en.wikipedia.org/wiki/Esri_grid

#include <limits>

#include "io/esri_header.h"
#include "util/ghost_array.h"

namespace io {
template <typename T> class esri {
  public:
	// load esri file into ghost_array
	static bool load(const std::string &path, ::util::ghost_array<T> &container);
	// variant that also checks if read header matches given on
	static bool load(const std::string &path, ::util::ghost_array<T> &container,
	                 const esri_header<T> &cmp_header);
};
}
