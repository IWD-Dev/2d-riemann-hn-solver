#include "stdafx.h"

#include "io/esri_header.h"
#include "util/algorithm.h"

namespace io {
// explicitly define valid template types
// restrain usage and enable exporting
template class esri_header<float_t>;
template class esri_header<double_t>;

template <typename T> typename esri_header<T> esri_header<T>::read_header(std::ifstream &file)
{
	std::string line;
	bool in_header = true;
	esri_header<T> header;

	while (in_header) {
		std::streampos oldpos = file.tellg();
		std::getline(file, line);
		std::streampos pos = file.tellg();

		if (line.empty()) {
			// skip whitespace
		}
		if (line.find("NCOLS") != std::string::npos) {
			header.ncols = std::stoi(util::get_right_of_delim(line, "NCOLS"));
		}
		else if (line.find("NROWS") != std::string::npos) {
			header.nrows = std::stoi(util::get_right_of_delim(line, "NROWS"));
		}
		else if (line.find("XLLCORNER") != std::string::npos) {
			header.xllcorner = ::util::to_numerical<T>(util::get_right_of_delim(line, "XLLCORNER"));
		}
		else if (line.find("YLLCORNER") != std::string::npos) {
			header.yllcorner = ::util::to_numerical<T>(util::get_right_of_delim(line, "YLLCORNER"));
		}
		else if (line.find("CELLSIZE") != std::string::npos) {
			header.cellsize = ::util::to_numerical<T>(util::get_right_of_delim(line, "CELLSIZE"));
		}
		else if (line.find("NODATA_value") != std::string::npos) {
			header.NODATA_value = std::stoi(util::get_right_of_delim(line, "NODATA_value"));
		}
		else {
			in_header = false;
			file.seekg(oldpos); // jump back to last line so next getline is
			                    // after header
		}
	}

	return header;
}

template <typename T> std::string esri_header<T>::header_to_string(const esri_header<T> &header)
{
	// parenthesis are just for nicer formatting
	return ("NCOLS " + std::to_string(header.ncols) + "\n") +
	       ("NROWS " + std::to_string(header.nrows) + "\n") +
	       ("XLLCORNER " + std::to_string(header.xllcorner) + "\n") +
	       ("YLLCORNER " + std::to_string(header.yllcorner) + "\n") +
	       ("CELLSIZE " + std::to_string(header.cellsize) + "\n") +
	       ("NODATA_value " + std::to_string(header.NODATA_value) + "\n");
}

template <typename T>
bool esri_header<T>::compare_headers(const esri_header<T> &h1, const esri_header<T> &h2)
{
	bool ret;
	ret = h1.ncols == h2.ncols;
	ret &= h1.nrows == h2.nrows;
	ret &= util::nearly_equal(h1.xllcorner, h2.xllcorner);
	ret &= util::nearly_equal(h1.yllcorner, h2.yllcorner);
	ret &= util::nearly_equal(h1.cellsize, h2.cellsize);
	ret &= h1.NODATA_value == h2.NODATA_value;
	return ret;
}
}
