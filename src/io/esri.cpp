#include "stdafx.h"

#include "io/esri.h"
#include "util/algorithm.h"

namespace io {

// explicitly define valid template types
// restrain usage and enable exporting
template class esri<float_t>;
template class esri<double_t>;

template <typename T> bool esri<T>::load(const std::string &path, util::ghost_array<T> &container)
{
	std::ios_base::sync_with_stdio(false);
	std::ifstream file;

	file.open(path, std::ios::in);
	if (file.is_open()) {

		spdlog::get("console")->info("Loading esri file: " + path + " ...");

		try {
			std::cout.precision(5);
			std::cout.setf(std::ios::fixed, std::ios::floatfield);

			esri_header<T> header = esri_header<T>::read_header(file);

			if (header.ncols == std::numeric_limits<size_t>::max() ||
			    header.nrows == std::numeric_limits<size_t>::max()) {
				spdlog::get("console")->error("ncols: " + std::to_string(header.ncols) + " or " +
				                              "nrows: " + std::to_string(header.nrows) +
				                              " invalid");
				return false;
			}

			std::string line;
			int line_num = 1;

			while (std::getline(file, line)) {
				std::vector<std::string> entries = util::split_string(line, '\t', header.ncols);
				if (entries.back().empty()) entries.pop_back(); // fix line ending with \t\n

				int col = 0;
				for (auto &entry : entries) { // same as < header.ncols
					container.at(col, line_num - 1) = ::util::to_numerical<T>(entry);
					++col;
				}
				++line_num;
			}

			file.close();
		}
		catch (...) { // TODO: make a proper catch
			spdlog::get("console")->error("Error in parsing File " + path);
			return false;
		}
	} // file not open
	else {
		spdlog::get("console")->error("could not open input file: " + path);
		return false;
	}

	std::ios_base::sync_with_stdio(true);
	spdlog::get("console")->info("Loading esri file: " + path + " done");

	return true;
}

template <typename T>
bool esri<T>::load(const std::string &path, util::ghost_array<T> &container,
                   const esri_header<T> &cmp_header)
{
	std::ios_base::sync_with_stdio(false);
	std::ifstream file;

	file.open(path, std::ios::in);
	if (file.is_open()) {

		spdlog::get("console")->info("Loading esri file: " + path + " ...");

		try {
			std::cout.precision(5);
			std::cout.setf(std::ios::fixed, std::ios::floatfield);

			esri_header<T> header = esri_header<T>::read_header(file);

			if (header.ncols == -1 || header.nrows == -1) {
				spdlog::get("console")->error("ncols: " + std::to_string(header.ncols) + " or " +
				                              "nrows: " + std::to_string(header.nrows) +
				                              " invalid");
				return false;
			}

			if (!esri_header<T>::compare_headers(header, cmp_header)) {
				spdlog::get("console")->error("esri_header does not match base esri_header!");
				return false;
			}

			std::string line;
			int line_num = 1;

			while (std::getline(file, line)) {
				std::vector<std::string> entries = util::split_string(line, '\t', header.ncols);
				if (entries.back().empty()) entries.pop_back(); // fix line ending with \t\n

				int col = 0;
				for (auto &entry : entries) { // same as < header.ncols
					container.at(col, line_num - 1) = ::util::to_numerical<T>(entry);
					++col;
				}
				++line_num;
			}

			file.close();
		}
		catch (...) { // TODO: make a proper catch
			spdlog::get("console")->error("Error in parsing File " + path);
			return false;
		}
	} // file not open
	else {
		spdlog::get("console")->error("could not open input file: " + path);
		return false;
	}

	std::ios_base::sync_with_stdio(true);
	spdlog::get("console")->info("Loading esri file: " + path + " done");

	return true;
}
}