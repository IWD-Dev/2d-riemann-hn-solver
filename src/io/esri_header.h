#pragma once

namespace io {
template <typename T> class esri_header {
  public:
	int ncols{-1};
	int nrows{-1};
	T xllcorner{T{0}};
	T yllcorner{T{0}};
	T cellsize{T{1}};
	int NODATA_value{-9999};

	// read esri file header
	// file streamposition is set to after header
	static esri_header<T> read_header(std::ifstream &file);

	static std::string header_to_string(const esri_header<T> &esri_header);

	// compare if two headers are equal
	static bool compare_headers(const esri_header<T> &h1, const esri_header<T> &h2);
};
}
