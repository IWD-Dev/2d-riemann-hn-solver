#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
//#include "windows.h"
#endif

#define FMT_HEADER_ONLY
#define FMT_USE_WINDOWS_H 0 // don't need windows system logging
#include "extern/spdlog/spdlog.h"

#include "extern/nlohmann/json.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

#define NOMINMAX

#define _USE_MATH_DEFINES
#include <cmath>

#include <string>
#include <vector>
#include <array>

#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
