#include "stdafx.h"

#include <omp.h>

#include "extern/cxxopts/cxxopts.h"
#include "util/logger.h"

#include "hn/simulation.h"

#define MAJOR_VERSION "1"
#define MINOR_VERSION "0"
#define PATCH_VERSION "0"

void print_help()
{
	// might replace with:
	// std::cout << options.help({"", "Group"}) << std::endl;
	std::cout << "Usage: \n";
	std::cout << "\t -c/--config FILE" << std::endl;
	std::cout << "\t -p/--precision [single|double]" << std::endl;
	std::cout << "\t -t/--threads NUMBER" << std::endl;
}

void cmd_options_init(cxxopts::Options &options)
{
	options.add_options()("c,config", "input configuration file", cxxopts::value<std::string>())(
	    "p,precision",
	    "specify precision used for calculation, valid options are \"single\" and \"double\"",
	    cxxopts::value<std::string>())("t,threads", "calculate in multiple threads",
	                                   cxxopts::value<int>()->implicit_value("0"));
}

std::pair<cxxopts::ParseResult, bool> cmd_options_parse_and_validate(cxxopts::Options &options,
                                                                     int argc, char *argv[])
{
	auto result = options.parse(argc, argv);
	bool valid = true;

	if (result.count("config") < 1) {
		spdlog::get("console")->error("No config file given as argument!");
		valid = false;
	}
	if (result.count("precision") < 1) {
		spdlog::get("console")->warn("No calculation precision given, using double!");
	}
	else {
		auto prc = result["precision"].as<std::string>();
		if (prc != "single" && prc != "double") {
			spdlog::get("console")->error("Precision argument invalid!");
			valid = false;
		}
	}
	if (result.count("threads") > 0) {
		auto tnum = result["threads"].as<int>();
		valid = tnum >= 0;
	}
	return std::make_pair(result, valid);
}

int main(int argc, char *argv[])
{
	std::cout << "2D Riemann Solver v" MAJOR_VERSION "." MINOR_VERSION "." PATCH_VERSION "\n"
	          << std::endl;

	if (argc < 0) {
		print_help();
		return 0;
	}

	util::log::setup();

	cxxopts::Options options("2D-Riemann-HN",
	                         "Two Dimensional Shallow-water Solver (Riemann equations)");
	cmd_options_init(options);

	auto args = cmd_options_parse_and_validate(options, argc, argv);
	if (!args.second) {
		spdlog::get("console")->error("Invalid arguments! Terminating Application.");
		return -3;
	}

	std::string args_concat = "";
	for (int i = 1; i < argc; ++i) {
		args_concat += std::string(argv[i]) + " ";
	}

	std::cout << "Arguments: " << args_concat << "\n" << std::endl;
	spdlog::get("console")->info("Starting Application ...");

	// could probably do this without code duplication
	if (args.first.count("precision") < 1 ||
	    args.first["precision"].as<std::string>() == "double") {

		auto sim = hn::simulation<double>();
		if (!sim.setup(args.first["config"].as<std::string>())) {
			spdlog::get("console")->error(
			    "Application failed to properly setup simulation. Terminating Application.");
			return -2;
		}
		if (args.first.count("threads") < 1) {
			if (!sim.run_sequential()) {
				spdlog::get("console")->error(
				    "An error occured while running the simulation. Terminating Application.");
				return -1;
			}
		}
		else {
			// setup amount of threads to use
			// might still vary in implementation due to omp constraints
			auto num_threads = args.first["threads"].as<int>();
			if (num_threads == 0) num_threads = omp_get_max_threads();
			omp_set_num_threads(num_threads);

			if (!sim.run_parallel()) {
				spdlog::get("console")->error(
				    "An error occured while running the simulation. Terminating Application.");
				return -1;
			}
		}
	}
	else if (args.first["precision"].as<std::string>() == "single") {
		auto sim = hn::simulation<float>();
		if (!sim.setup(args.first["config"].as<std::string>())) {
			spdlog::get("console")->error(
			    "Application failed to properly setup simulation. Terminating Application.");
			return -2;
		}
		if (args.first.count("threads") < 1) {
			if (!sim.run_sequential()) {
				spdlog::get("console")->error(
				    "An error occured while running the simulation. Terminating Application.");
				return -1;
			}
		}
		else {
			// TODO: setup omp thread number
			auto num_threads = args.first["threads"].as<int>();
			if (!sim.run_parallel()) {
				spdlog::get("console")->error(
				    "An error occured while running the simulation. Terminating Application.");
				return -1;
			}
		}
	}

	spdlog::get("console")->info("Application ran successfully. Closing Application.");
	return 0;
}
