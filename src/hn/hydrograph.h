#pragma once

#include <vector>
#include <string>

namespace hn {
template <typename T> class hydrograph {
  public:
	std::string name;

	// time, value
	std::vector<std::pair<T, T>> steps;

	static typename std::vector<hydrograph<T>>::const_iterator
	get_hydrograph_by_name(const std::string &name, const std::vector<hydrograph<T>> &hydrographs);

	T interpolate_linear(T time);

	// check if timesteps reach at least up to max_time
	bool is_valid(T max_time) const;
};
}
