#pragma once

#include <array>

#include "glm/glm.hpp"

#include "hn/reconstruction.h"
#include "hn/wetdry.h"

#include "hn/state.h"

namespace hn {
// class holding static hn calculation functions
// template with only static -> less code writing
template <typename T> class calculations {
  public:
	// xflux + yflux in
	// return 0 - east, 1 - west, 2 - north, 3 - south
	static std::array<glm::tvec3<T, glm::precision::highp>, 4> flux(const reconstruction<T> &recon,
	                                                                const wetdry<T> &wd);

	static void updatePrimitives(state<T> &state, const util::ghost_array<T> &dem,
	                             util::ghost_array<T> &height_carry,
	                             const glm::tvec3<T, glm::precision::highp> Up, int x, int y);

  private:
	static glm::tvec3<T, glm::precision::highp>
	xflux_intern(const T &uL, const T &uR, const T &vL, const T &vR, const T &qxL, const T &qxR,
	             const T &qyL, const T &qyR, const T &hL, const T &hR, const T &etaL, const T &etaR,
	             const T &zbface);

	static glm::tvec3<T, glm::precision::highp>
	yflux_intern(const T &uD, const T &uU, const T &vD, const T &vU, const T &qxD, const T &qxU,
	             const T &qyD, const T &qyU, const T &hD, const T &hU, const T &etaD, const T &etaU,
	             const T &zbface);
};

template <typename T> class calculations_parallel {
  public:
	static void updatePrimitives(state<T> &state, const util::ghost_array<T> &dem,
	                             util::ghost_array<T> &height_carry, int x, int y);

	static glm::tvec3<T, glm::precision::highp> flux_east(const reconstruction_parallel<T> &recon,
	                                                      const wetdry_parallel<T> &wd, const int x,
	                                                      const int y);

	static glm::tvec3<T, glm::precision::highp> flux_west(const reconstruction_parallel<T> &recon,
	                                                      const wetdry_parallel<T> &wd, const int x,
	                                                      const int y);

	static glm::tvec3<T, glm::precision::highp> flux_north(const reconstruction_parallel<T> &recon,
	                                                       const wetdry_parallel<T> &wd,
	                                                       const int x, const int y);

	static glm::tvec3<T, glm::precision::highp> flux_south(const reconstruction_parallel<T> &recon,
	                                                       const wetdry_parallel<T> &wd,
	                                                       const int x, const int y);

  private:
	static glm::tvec3<T, glm::precision::highp>
	xflux_intern(const util::ghost_array<T> &uL, const util::ghost_array<T> &uR,
	             const util::ghost_array<T> &vL, const util::ghost_array<T> &vR,
	             const util::ghost_array<T> &qxL, const util::ghost_array<T> &qxR,
	             const util::ghost_array<T> &qyL, const util::ghost_array<T> &qyR,
	             const util::ghost_array<T> &hL, const util::ghost_array<T> &hR,
	             const util::ghost_array<T> &etaL, const util::ghost_array<T> &etaR,
	             const util::ghost_array<T> &zbface, const int x, const int y);

	static glm::tvec3<T, glm::precision::highp>
	yflux_intern(const util::ghost_array<T> &uD, const util::ghost_array<T> &uU,
	             const util::ghost_array<T> &vD, const util::ghost_array<T> &vU,
	             const util::ghost_array<T> &qxD, const util::ghost_array<T> &qxU,
	             const util::ghost_array<T> &qyD, const util::ghost_array<T> &qyU,
	             const util::ghost_array<T> &hD, const util::ghost_array<T> &hU,
	             const util::ghost_array<T> &etaD, const util::ghost_array<T> &etaU,
	             const util::ghost_array<T> &zbface, const int x, const int y);
};
} // namespace hn
