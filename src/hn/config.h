#pragma once

#include <string>
#include <vector>

#include "extern/nlohmann/json.h"

#include "hn/boundary.h"

namespace hn {
template <typename T> class config {
	// holding application config variables
	// using all the compiler generated ctor, dtor, copyass, etc...

  public:
	// load config from json
	bool load_from_json(const std::string &path);

	std::string to_string();

	nlohmann::json json;

	/* ----------- Simulation Config -----------*/

	// time till simulation stops in seconds
	T time_stop{};

	// time step additive increase
	T time_step_AIMD_increase{};

	// time step multiplicative decrease
	T time_step_AIMD_penalty{};

	// time step multiplicative increase
	T time_step_AIMD_bonus{};

	// minimum time step, enforce if dt degenerates
	T time_step_min{};

	// maximum time step, limit if dt gets too high
	T time_step_max{};

	// # of loop iterations how often files are written to HDD
	T time_sim_out_file{};

	// # of loop iterations how often is printed on console
	T time_sim_console_log{};

	// time step how long between save states
	T time_rollback{};

	// maximum number of save states
	size_t rollback_count{};

	// maximum number of possible rollbacks
	size_t rollback_strikes{};

	// lower and upper bound of courant tolerance
	// https://de.wikipedia.org/wiki/CFL-Zahl
	T courant_tolerance_high{};
	T courant_tolerance_low{};

	/* ----------- IO - IN Config -----------*/

	// TODO: when using in simulation, pass as const reference
	std::vector<hydrograph<T>> hydrographs;

	// simple storage for bcs
	// easier to access with boundary_condition_accessor
	std::vector<boundary::boundary_condition<T>> boundary_conditions;

	// file path containing dem file
	// _must_ be set
	std::string in_dem{};

	// file path containing inital water level file
	// can be same as dem for dry initial conditions
	// _must_ be set
	std::string in_eta{};

	// file path containing x-velocity file
	// can be emtpy -> all x-velocity should be 0
	std::string in_x_velocity{};

	// file path containing y-velocity file
	// can be emtpy -> all x-velocity should be 0
	std::string in_y_velocity{};

	// file path containing manning file
	// TODO: get to know if it can be empty???
	std::string in_manning{};

	// Ctolmax
	// Ctolmin

	/* ----------- IO - OUT Config -----------*/

	// prefix for result files
	std::string result_prefix{};

	// direcory path to write to
	std::string out_path{};

	// write depth if true
	bool out_depth{};

	// write water level if true
	bool out_height{};

	// write x-velocity if true
	bool out_x_velocity{};

	// write y-velocity if hortentrue
	bool out_y_velocity{};

	// write velocity magnitude if true
	bool out_vmag{};

	// write flow direction (in degrees) if true
	bool out_flowd{};
};
} // namespace hn
