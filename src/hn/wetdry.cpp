#include "stdafx.h"

#include "hn/wetdry.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class wetdry<float_t>;
template class wetdry<double_t>;
template class wetdry_parallel<float_t>;
template class wetdry_parallel<double_t>;

template <typename T> void wetdry<T>::wet_dry(const reconstruction<T> &recon)
{
	// east
	wet_dry_intern(recon.etaLe, recon.etaRe, this->etaLeWD, this->etaReWD, this->hLeWD, this->hReWD,
	               this->qxLeWD, this->qxReWD, this->qyLeWD, this->qyReWD, recon.uLe, recon.uRe,
	               recon.vLe, recon.vRe, recon.zbLe, recon.zbRe, this->zbe);

	// west
	wet_dry_intern(recon.etaRw, recon.etaLw, this->etaRwWD, this->etaLwWD, this->hRwWD, this->hLwWD,
	               this->qxRwWD, this->qxLwWD, this->qyRwWD, this->qyLwWD, recon.uRw, recon.uLw,
	               recon.vRw, recon.vLw, recon.zbRw, recon.zbLw, this->zbw);

	// north
	wet_dry_intern(recon.etaDn, recon.etaUn, this->etaDnWD, this->etaUnWD, this->hDnWD, this->hUnWD,
	               this->qxDnWD, this->qxUnWD, this->qyDnWD, this->qyUnWD, recon.uDn, recon.uUn,
	               recon.vDn, recon.vUn, recon.zbDn, recon.zbUn, this->zbn);

	// south
	wet_dry_intern(recon.etaUs, recon.etaDs, this->etaUsWD, this->etaDsWD, this->hUsWD, this->hDsWD,
	               this->qxUsWD, this->qxDsWD, this->qyUsWD, this->qyDsWD, recon.uUs, recon.uDs,
	               recon.vUs, recon.vDs, recon.zbUs, recon.zbDs, this->zbs);
}

template <typename T>
void wetdry<T>::wet_dry_intern(const T &etaL, const T &etaR, T &etaLWD, T &etaRWD, T &hLWD, T &hRWD,
                               T &qxLWD, T &qxRWD, T &qyLWD, T &qyRWD, const T &uL, const T &uR,
                               const T &vL, const T &vR, const T &zbL, const T &zbR, T &zbface)
{
	zbface = std::max(zbL, zbR);
	hLWD = std::max(T{0}, (etaL - zbface));
	hRWD = std::max(T{0}, (etaR - zbface));
	etaLWD = hLWD + zbface;
	etaRWD = hRWD + zbface;
	qxLWD = uL * hLWD;
	qxRWD = uR * hRWD;
	qyLWD = vL * hLWD;
	qyRWD = vR * hRWD;

	T dz = std::max(T{0}, (zbface - etaL));
	zbface -= dz;
	etaLWD -= dz;
	etaRWD -= dz;
}

template <typename T> wetdry_parallel<T>::wetdry_parallel(int ncols, int nrows)
{
	etaLeWD = {ncols, nrows};
	etaReWD = {ncols, nrows};
	etaLwWD = {ncols, nrows};
	etaRwWD = {ncols, nrows};
	etaDnWD = {ncols, nrows};
	etaUnWD = {ncols, nrows};
	etaDsWD = {ncols, nrows};
	etaUsWD = {ncols, nrows};
	hLeWD = {ncols, nrows};
	hReWD = {ncols, nrows};
	hLwWD = {ncols, nrows};
	hRwWD = {ncols, nrows};
	hDnWD = {ncols, nrows};
	hUnWD = {ncols, nrows};
	hDsWD = {ncols, nrows};
	hUsWD = {ncols, nrows};
	qxLeWD = {ncols, nrows};
	qxReWD = {ncols, nrows};
	qxLwWD = {ncols, nrows};
	qxRwWD = {ncols, nrows};
	qxDnWD = {ncols, nrows};
	qxUnWD = {ncols, nrows};
	qxDsWD = {ncols, nrows};
	qxUsWD = {ncols, nrows};
	qyLeWD = {ncols, nrows};
	qyReWD = {ncols, nrows};
	qyLwWD = {ncols, nrows};
	qyRwWD = {ncols, nrows};
	qyDnWD = {ncols, nrows};
	qyUnWD = {ncols, nrows};
	qyDsWD = {ncols, nrows};
	qyUsWD = {ncols, nrows};
	zbe = {ncols, nrows};
	zbw = {ncols, nrows};
	zbn = {ncols, nrows};
	zbs = {ncols, nrows};
}
template <typename T>
void wetdry_parallel<T>::wet_dry_east(const reconstruction_parallel<T> &recon, const int x,
                                      const int y)
{
	wet_dry_intern(recon.etaLe, recon.etaRe, this->etaLeWD, this->etaReWD, this->hLeWD, this->hReWD,
	               this->qxLeWD, this->qxReWD, this->qyLeWD, this->qyReWD, recon.uLe, recon.uRe,
	               recon.vLe, recon.vRe, recon.zbLe, recon.zbRe, this->zbe, x, y);
}

template <typename T>
void wetdry_parallel<T>::wet_dry_west(const reconstruction_parallel<T> &recon, const int x,
                                      const int y)
{
	wet_dry_intern(recon.etaRw, recon.etaLw, this->etaRwWD, this->etaLwWD, this->hRwWD, this->hLwWD,
	               this->qxRwWD, this->qxLwWD, this->qyRwWD, this->qyLwWD, recon.uRw, recon.uLw,
	               recon.vRw, recon.vLw, recon.zbRw, recon.zbLw, this->zbw, x, y);
}

template <typename T>
void wetdry_parallel<T>::wet_dry_north(const reconstruction_parallel<T> &recon, const int x,
                                       const int y)
{
	wet_dry_intern(recon.etaDn, recon.etaUn, this->etaDnWD, this->etaUnWD, this->hDnWD, this->hUnWD,
	               this->qxDnWD, this->qxUnWD, this->qyDnWD, this->qyUnWD, recon.uDn, recon.uUn,
	               recon.vDn, recon.vUn, recon.zbDn, recon.zbUn, this->zbn, x, y);
}

template <typename T>
void wetdry_parallel<T>::wet_dry_south(const reconstruction_parallel<T> &recon, const int x,
                                       const int y)
{
	wet_dry_intern(recon.etaUs, recon.etaDs, this->etaUsWD, this->etaDsWD, this->hUsWD, this->hDsWD,
	               this->qxUsWD, this->qxDsWD, this->qyUsWD, this->qyDsWD, recon.uUs, recon.uDs,
	               recon.vUs, recon.vDs, recon.zbUs, recon.zbDs, this->zbs, x, y);
}

template <typename T>
void wetdry_parallel<T>::wet_dry_intern(
    const util::ghost_array<T> &etaL, const util::ghost_array<T> &etaR,
    util::ghost_array<T> &etaLWD, util::ghost_array<T> &etaRWD, util::ghost_array<T> &hLWD,
    util::ghost_array<T> &hRWD, util::ghost_array<T> &qxLWD, util::ghost_array<T> &qxRWD,
    util::ghost_array<T> &qyLWD, util::ghost_array<T> &qyRWD, const util::ghost_array<T> &uL,
    const util::ghost_array<T> &uR, const util::ghost_array<T> &vL, const util::ghost_array<T> &vR,
    const util::ghost_array<T> &zbL, const util::ghost_array<T> &zbR, util::ghost_array<T> &zbface,
    int x, int y)
{
	T zbf = std::max(zbL(x, y), zbR(x, y));
	T hlwd = std::max(T{0}, (etaL(x, y) - zbf));
	T hrwd = std::max(T{0}, (etaR(x, y) - zbf));
	zbface(x, y) = zbf;
	hLWD(x, y) = hlwd;
	hRWD(x, y) = hrwd;
	etaLWD(x, y) = hlwd + zbf;
	etaRWD(x, y) = hrwd + zbf;
	qxLWD(x, y) = uL(x, y) * hlwd;
	qxRWD(x, y) = uR(x, y) * hrwd;
	qyLWD(x, y) = vL(x, y) * hlwd;
	qyRWD(x, y) = vR(x, y) * hrwd;

	T dz = std::max(T{0}, (zbf - etaL(x, y)));
	zbface(x, y) -= dz;
	etaLWD(x, y) -= dz;
	etaRWD(x, y) -= dz;
}
} // namespace hn
