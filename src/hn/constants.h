#pragma once

namespace hn {
// constants just in highest precision (therefore here double)
// typecast with static_cast<T> (for compile-time assertion) or
//		T(x), T{x} for simple conversion
const double G = 9.81;
const double G_2 = 9.81 * 0.5;
const double G_INV = 1.0 / 9.81;
const double DRY = 2.0 * 1e-3;
const double HCARRY_THRESHOLD = 1e-3;
}