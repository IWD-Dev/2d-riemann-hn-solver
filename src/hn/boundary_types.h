#pragma once

namespace hn {
namespace boundary {
	// forward decl
	template <typename T> class boundary_condition;

	// default closed
	enum class type_t { closed, open, discharge, eta, unassigned };

	// default unassigned
	enum class edge_t { unassigned, N, E, S, W };
}
}
