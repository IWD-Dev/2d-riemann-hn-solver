#include "stdafx.h"

#include "hn/calculations.h"
#include "hn/constants.h"
#include "util/algorithm.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class calculations<float_t>;
template class calculations<double_t>;
template class calculations_parallel<float_t>;
template class calculations_parallel<double_t>;

template <typename T>
std::array<glm::tvec3<T, glm::precision::highp>, 4>
calculations<T>::flux(const reconstruction<T> &recon, const wetdry<T> &wd)
{
	std::array<glm::tvec3<T, glm::precision::highp>, 4> fluxes{};

	// east
	fluxes[0] =
	    xflux_intern(recon.uLe, recon.uRe, recon.vLe, recon.vRe, wd.qxLeWD, wd.qxReWD, wd.qyLeWD,
	                 wd.qyReWD, wd.hLeWD, wd.hReWD, wd.etaLeWD, wd.etaReWD, wd.zbe);

	// west
	fluxes[1] =
	    xflux_intern(recon.uLw, recon.uRw, recon.vLw, recon.vRw, wd.qxLwWD, wd.qxRwWD, wd.qyLwWD,
	                 wd.qyRwWD, wd.hLwWD, wd.hRwWD, wd.etaLwWD, wd.etaRwWD, wd.zbw);

	// north
	fluxes[2] =
	    yflux_intern(recon.uDn, recon.uUn, recon.vDn, recon.vUn, wd.qxDnWD, wd.qxUnWD, wd.qyDnWD,
	                 wd.qyUnWD, wd.hDnWD, wd.hUnWD, wd.etaDnWD, wd.etaUnWD, wd.zbn);

	// south
	fluxes[3] =
	    yflux_intern(recon.uDs, recon.uUs, recon.vDs, recon.vUs, wd.qxDsWD, wd.qxUsWD, wd.qyDsWD,
	                 wd.qyUsWD, wd.hDsWD, wd.hUsWD, wd.etaDsWD, wd.etaUsWD, wd.zbs);

	return fluxes;
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations<T>::xflux_intern(const T &uL, const T &uR, const T &vL, const T &vR, const T &qxL,
                              const T &qxR, const T &qyL, const T &qyR, const T &hL, const T &hR,
                              const T &etaL, const T &etaR, const T &zbface)
{
	glm::tvec3<T, glm::precision::highp> FL{}, FR{}, UL{}, UR{};
	T SL{}, SR{};

	// compute face values of the vectors
	FL[0] = qxL;
	FL[1] = hL * util::sqr(uL) + T(G_2) * (util::sqr(etaL) - T{2} * etaL * zbface);
	FL[2] = uL * qyL;

	FR[0] = qxR;
	FR[1] = hR * util::sqr(uR) + T(G_2) * (util::sqr(etaR) - T{2} * etaR * zbface);
	FR[2] = uR * qyR;

	UL[0] = etaL;
	UL[1] = qxL;
	UL[2] = qyL;

	UR[0] = etaR;
	UR[1] = qxR;
	UR[2] = qyR;

	// compute wave velocities
	T ustar = T(0.5) * (uL + uR) + std::sqrt(T(G) * hL) - std::sqrt(T(G) * hR);
	T hstar = T(G_INV) * util::sqr(T(0.5) * (std::sqrt(T(G) * hL) + std::sqrt(T(G) * hR)) +
	                               T(0.25) * (uL - uR));

	if (hL <= T(DRY))
		SL = uR - T{2} * std::sqrt(T(G) * hR);
	else
		SL = std::min(uL - std::sqrt(T(G) * hL), ustar - std::sqrt(T(G) * hstar));

	if (hR <= T(DRY))
		SR = uL + T{2} * std::sqrt(T(G) * hL);
	else
		SR = std::max(uR + std::sqrt(T(G) * hR), ustar + std::sqrt(T(G) * hstar));

	glm::tvec3<T, glm::precision::highp> FstarL{}, FstarR{};

	T Sstar = (SL * hR * (uR - SR) - SR * hL * (uL - SL)) / (hR * (uR - SR) - hL * (uL - SL));
	Sstar = Sstar != Sstar ? T{0} : Sstar; // fix division by zero
	FstarL[0] = (SR * FL[0] - SL * FR[0] + SL * SR * (UR[0] - UL[0])) / (SR - SL);
	FstarL[1] = (SR * FL[1] - SL * FR[1] + SL * SR * (UR[1] - UL[1])) / (SR - SL);
	FstarL[0] = FstarL[0] != FstarL[0] ? T{0} : FstarL[0]; // fix division by zero
	FstarL[1] = FstarL[1] != FstarL[1] ? T{0} : FstarL[1]; // fix division by zero

	if (SL <= T{0} && T{0} <= Sstar) {
		FstarL[2] = vL * FstarL[0];
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		FstarL[2] = vR * FstarL[0];
	}

	FstarR[0] = FstarL[0];
	FstarR[1] = FstarL[1];
	if (SL <= T{0} && T{0} <= Sstar) {
		FstarR[2] = vL * FstarR[0];
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		FstarR[2] = vR * FstarR[0];
	}

	// assign Fe according to Riemann solution structure
	if (T{0} <= SL) {
		return FL;
	}
	else if (SL <= T{0} && T{0} <= Sstar) {
		return FstarL;
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		return FstarR;
	}
	else {
		return FR;
	}
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations<T>::yflux_intern(const T &uD, const T &uU, const T &vD, const T &vU, const T &qxD,
                              const T &qxU, const T &qyD, const T &qyU, const T &hD, const T &hU,
                              const T &etaD, const T &etaU, const T &zbface)
{
	glm::tvec3<T, glm::precision::highp> GD{}, GU{}, UD{}, UU{};
	T SD{}, SU{};

	// compute face values of the vectors
	GD[0] = qyD;
	GD[1] = vD * qxD;
	GD[2] = hD * util::sqr(vD) + T(G_2) * (util::sqr(etaD) - T{2} * etaD * zbface);

	GU[0] = qyU;
	GU[1] = vU * qxU;
	GU[2] = hU * util::sqr(vU) + T(G_2) * (util::sqr(etaU) - T{2} * etaU * zbface);

	UD[0] = etaD;
	UD[1] = qxD;
	UD[2] = qyD;

	UU[0] = etaU;
	UU[1] = qxU;
	UU[2] = qyU;

	// compute wave velocities
	T vstar = T(0.5) * (vD + vU) + std::sqrt(T(G) * hD) - std::sqrt(T(G) * hU);
	T hstar = T(G_INV) * util::sqr(T(0.5) * (std::sqrt(T(G) * hD) + std::sqrt(T(G) * hU)) +
	                               T(0.25) * (vD - vU));

	if (hD <= T(DRY))
		SD = vU - T{2} * std::sqrt(T(G) * hU);
	else
		SD = std::min(vD - std::sqrt(T(G) * hD), vstar - std::sqrt(T(G) * hstar));

	if (hU <= T(DRY))
		SU = vD + T{2} * std::sqrt(T(G) * hD);
	else
		SU = std::max(vU + std::sqrt(T(G) * hU), vstar + std::sqrt(T(G) * hstar));

	glm::tvec3<T, glm::precision::highp> GstarD{}, GstarU{};

	T Sstar = (SD * hU * (vU - SU) - SU * hD * (vD - SD)) / (hU * (vU - SU) - hD * (vD - SD));
	Sstar = Sstar != Sstar ? T{0} : Sstar; // fix division by zero
	GstarD[0] = (SU * GD[0] - SD * GU[0] + SD * SU * (UU[0] - UD[0])) / (SU - SD);
	GstarD[2] = (SU * GD[2] - SD * GU[2] + SD * SU * (UU[2] - UD[2])) / (SU - SD);
	GstarD[0] = GstarD[0] != GstarD[0] ? T{0} : GstarD[0]; // fix division by zero
	GstarD[1] = GstarD[1] != GstarD[1] ? T{0} : GstarD[1]; // fix division by zero

	if (SD <= T{0} && T{0} <= Sstar) {
		GstarD[1] = uD * GstarD[0];
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		GstarD[1] = uU * GstarD[0];
	}

	GstarU[0] = GstarD[0];
	GstarU[2] = GstarD[2];
	if (SD <= T{0} && T{0} <= Sstar) {
		GstarU[1] = uD * GstarU[0];
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		GstarU[1] = uU * GstarU[0];
	}

	// assign Fe according to Riemann solution structure
	if (T{0} <= SD) {
		return GD;
	}
	else if (SD <= T{0} && T{0} <= Sstar) {
		return GstarD;
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		return GstarU;
	}
	else {
		return GU;
	}
}
template <typename T>
void calculations<T>::updatePrimitives(state<T> &state, const util::ghost_array<T> &dem,
                                       util::ghost_array<T> &height_carry,
                                       const glm::tvec3<T, glm::precision::highp> Up, int x, int y)
{
	// preliminary update of state variables
	state.height(x, y) = Up[0];
	state.x_flow(x, y) = Up[1];
	state.y_flow(x, y) = Up[2];
	state.depth(x, y) = state.height(x, y) - dem(x, y);

	auto h = state.depth(x, y);
	if (h <= T(DRY)) {

		// if cell dry, velocities and fluxes are zero
		state.x_velocity(x, y) = T{0};
		state.y_velocity(x, y) = T{0};
		state.x_flow(x, y) = T{0};
		state.y_flow(x, y) = T{0};

		// if h was negative, find neighbor with the most water and subtract the negative h from it
		// to ensure mass conservation.
		// this is done only if after subtraction that neighbor has a depth > 1e-6 m. otherwise,
		// negative water depth carried onto next
		// iteration until suitable neighbor is identified or the cell is sufficiently recharged

		if (h < T{0}) {
			// spdlog::get("console")->warn("hcarry");
			height_carry(x, y) += h;

			// find the right neighbor
			int ip{}, jp{};
			std::vector<T> neighh{state.depth(x + 1, y), state.depth(x - 1, y),
			                      state.depth(x, y - 1), state.depth(x, y + 1)};

			// if multiple neighbors have same depth, chose a random one
			std::vector<int> neighhpos{};
			T nh_max_value = neighh[0];
			neighhpos.push_back(0);
			for (int i = 1; i < 4; ++i) {
				if (neighh[i] > nh_max_value) {
					neighhpos.clear();
					neighhpos.push_back(i);
					nh_max_value = neighh[i];
				}
				else if (neighh[i] == nh_max_value) {
					neighhpos.push_back(i);
				}
			}
			auto nhmax = neighhpos[std::rand() % neighhpos.size()];

			switch (nhmax) {
			case 0:
				jp = 0;
				ip = 1;
				break;
			case 1:
				jp = 0;
				ip = -1;
				break;
			case 2:
				jp = -1;
				ip = 0;
				break;
			case 3:
				jp = 1;
				ip = 0;
				break;
			}

			// if neighbor has enough water, take the negative water depth from it
			if ((state.depth(x + ip, y + jp) + height_carry(x, y)) > T(HCARRY_THRESHOLD)) {

				// adjust qx and qy to keep u and v unmodified
				state.x_flow(x + ip, y + jp) = state.x_flow(x + ip, y + jp) *
				                               (state.depth(x + ip, y + jp) + height_carry(x, y)) /
				                               state.depth(x + ip, y + jp);
				state.y_flow(x + ip, y + jp) = state.y_flow(x + ip, y + jp) *
				                               (state.depth(x + ip, y + jp) + height_carry(x, y)) /
				                               state.depth(x + ip, y + jp);

				// remove water and reset hcarry
				state.depth(x + ip, y + jp) += height_carry(x, y); // hcarry is negative
				height_carry(x, y) = T{0};
			}
		}

		// "fill" negative depth
		state.depth(x, y) = T{0};
		state.height(x, y) = dem(x, y);
	}
	else {
		state.x_velocity(x, y) = state.x_flow(x, y) / state.depth(x, y);
		state.y_velocity(x, y) = state.y_flow(x, y) / state.depth(x, y);
	}
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations_parallel<T>::flux_east(const reconstruction_parallel<T> &recon,
                                    const wetdry_parallel<T> &wd, const int x, const int y)
{
	return xflux_intern(recon.uLe, recon.uRe, recon.vLe, recon.vRe, wd.qxLeWD, wd.qxReWD, wd.qyLeWD,
	                    wd.qyReWD, wd.hLeWD, wd.hReWD, wd.etaLeWD, wd.etaReWD, wd.zbe, x, y);
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations_parallel<T>::flux_west(const reconstruction_parallel<T> &recon,
                                    const wetdry_parallel<T> &wd, const int x, const int y)
{
	return xflux_intern(recon.uLw, recon.uRw, recon.vLw, recon.vRw, wd.qxLwWD, wd.qxRwWD, wd.qyLwWD,
	                    wd.qyRwWD, wd.hLwWD, wd.hRwWD, wd.etaLwWD, wd.etaRwWD, wd.zbw, x, y);
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations_parallel<T>::flux_north(const reconstruction_parallel<T> &recon,
                                     const wetdry_parallel<T> &wd, const int x, const int y)
{
	return yflux_intern(recon.uDn, recon.uUn, recon.vDn, recon.vUn, wd.qxDnWD, wd.qxUnWD, wd.qyDnWD,
	                    wd.qyUnWD, wd.hDnWD, wd.hUnWD, wd.etaDnWD, wd.etaUnWD, wd.zbn, x, y);
}

template <typename T>
glm::tvec3<T, glm::precision::highp>
calculations_parallel<T>::flux_south(const reconstruction_parallel<T> &recon,
                                     const wetdry_parallel<T> &wd, const int x, const int y)
{
	return yflux_intern(recon.uDs, recon.uUs, recon.vDs, recon.vUs, wd.qxDsWD, wd.qxUsWD, wd.qyDsWD,
	                    wd.qyUsWD, wd.hDsWD, wd.hUsWD, wd.etaDsWD, wd.etaUsWD, wd.zbs, x, y);
}

template <typename T>
glm::tvec3<T, glm::precision::highp> calculations_parallel<T>::xflux_intern(
    const util::ghost_array<T> &uL, const util::ghost_array<T> &uR, const util::ghost_array<T> &vL,
    const util::ghost_array<T> &vR, const util::ghost_array<T> &qxL,
    const util::ghost_array<T> &qxR, const util::ghost_array<T> &qyL,
    const util::ghost_array<T> &qyR, const util::ghost_array<T> &hL, const util::ghost_array<T> &hR,
    const util::ghost_array<T> &etaL, const util::ghost_array<T> &etaR,
    const util::ghost_array<T> &zbface, const int x, const int y)
{
	// TODO: Fetch memory once and calculate locally

	glm::tvec3<T, glm::precision::highp> FL{}, FR{}, UL{}, UR{};
	T SL{}, SR{};

	// compute face values of the vectors
	FL[0] = qxL(x, y);
	FL[1] = hL(x, y) * util::sqr(uL(x, y)) +
	        T(G_2) * (util::sqr(etaL(x, y)) - T{2} * etaL(x, y) * zbface(x, y));
	FL[2] = uL(x, y) * qyL(x, y);

	FR[0] = qxR(x, y);
	FR[1] = hR(x, y) * util::sqr(uR(x, y)) +
	        T(G_2) * (util::sqr(etaR(x, y)) - T{2} * etaR(x, y) * zbface(x, y));
	FR[2] = uR(x, y) * qyR(x, y);

	UL[0] = etaL(x, y);
	UL[1] = qxL(x, y);
	UL[2] = qyL(x, y);

	UR[0] = etaR(x, y);
	UR[1] = qxR(x, y);
	UR[2] = qyR(x, y);

	// compute wave velocities
	T ustar =
	    T(0.5) * (uL(x, y) + uR(x, y)) + std::sqrt(T(G) * hL(x, y)) - std::sqrt(T(G) * hR(x, y));
	T hstar =
	    T(G_INV) * util::sqr(T(0.5) * (std::sqrt(T(G) * hL(x, y)) + std::sqrt(T(G) * hR(x, y))) +
	                         T(0.25) * (uL(x, y) - uR(x, y)));

	if (hL(x, y) <= T(DRY))
		SL = uR(x, y) - T{2} * std::sqrt(T(G) * hR(x, y));
	else
		SL = std::min(uL(x, y) - std::sqrt(T(G) * hL(x, y)), ustar - std::sqrt(T(G) * hstar));

	if (hR(x, y) <= T(DRY))
		SR = uL(x, y) + T{2} * std::sqrt(T(G) * hL(x, y));
	else
		SR = std::max(uR(x, y) + std::sqrt(T(G) * hR(x, y)), ustar + std::sqrt(T(G) * hstar));

	glm::tvec3<T, glm::precision::highp> FstarL{}, FstarR{};

	T Sstar = (SL * hR(x, y) * (uR(x, y) - SR) - SR * hL(x, y) * (uL(x, y) - SL)) /
	          (hR(x, y) * (uR(x, y) - SR) - hL(x, y) * (uL(x, y) - SL));
	Sstar = Sstar != Sstar ? T{0} : Sstar; // fix division by zero
	FstarL[0] = (SR * FL[0] - SL * FR[0] + SL * SR * (UR[0] - UL[0])) / (SR - SL);
	FstarL[1] = (SR * FL[1] - SL * FR[1] + SL * SR * (UR[1] - UL[1])) / (SR - SL);
	FstarL[0] = FstarL[0] != FstarL[0] ? T{0} : FstarL[0]; // fix division by zero
	FstarL[1] = FstarL[1] != FstarL[1] ? T{0} : FstarL[1]; // fix division by zero

	if (SL <= T{0} && T{0} <= Sstar) {
		FstarL[2] = vL(x, y) * FstarL[0];
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		FstarL[2] = vR(x, y) * FstarL[0];
	}

	FstarR[0] = FstarL[0];
	FstarR[1] = FstarL[1];
	if (SL <= T{0} && T{0} <= Sstar) {
		FstarR[2] = vL(x, y) * FstarR[0];
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		FstarR[2] = vR(x, y) * FstarR[0];
	}

	// assign Fe according to Riemann solution structure
	if (T{0} <= SL) {
		return FL;
	}
	else if (SL <= T{0} && T{0} <= Sstar) {
		return FstarL;
	}
	else if (Sstar <= T{0} && T{0} <= SR) {
		return FstarR;
	}
	else {
		return FR;
	}
}

template <typename T>
glm::tvec3<T, glm::precision::highp> calculations_parallel<T>::yflux_intern(
    const util::ghost_array<T> &uD, const util::ghost_array<T> &uU, const util::ghost_array<T> &vD,
    const util::ghost_array<T> &vU, const util::ghost_array<T> &qxD,
    const util::ghost_array<T> &qxU, const util::ghost_array<T> &qyD,
    const util::ghost_array<T> &qyU, const util::ghost_array<T> &hD, const util::ghost_array<T> &hU,
    const util::ghost_array<T> &etaD, const util::ghost_array<T> &etaU,
    const util::ghost_array<T> &zbface, const int x, const int y)
{
	glm::tvec3<T, glm::precision::highp> GD{}, GU{}, UD{}, UU{};
	T SD{}, SU{};

	// compute face values of the vectors
	GD[0] = qyD(x, y);
	GD[1] = vD(x, y) * qxD(x, y);
	GD[2] = hD(x, y) * util::sqr(vD(x, y)) +
	        T(G_2) * (util::sqr(etaD(x, y)) - T{2} * etaD(x, y) * zbface(x, y));

	GU[0] = qyU(x, y);
	GU[1] = vU(x, y) * qxU(x, y);
	GU[2] = hU(x, y) * util::sqr(vU(x, y)) +
	        T(G_2) * (util::sqr(etaU(x, y)) - T{2} * etaU(x, y) * zbface(x, y));

	UD[0] = etaD(x, y);
	UD[1] = qxD(x, y);
	UD[2] = qyD(x, y);

	UU[0] = etaU(x, y);
	UU[1] = qxU(x, y);
	UU[2] = qyU(x, y);

	// compute wave velocities
	T vstar =
	    T(0.5) * (vD(x, y) + vU(x, y)) + std::sqrt(T(G) * hD(x, y)) - std::sqrt(T(G) * hU(x, y));
	T hstar =
	    T(G_INV) * util::sqr(T(0.5) * (std::sqrt(T(G) * hD(x, y)) + std::sqrt(T(G) * hU(x, y))) +
	                         T(0.25) * (vD(x, y) - vU(x, y)));

	if (hD(x, y) <= T(DRY))
		SD = vU(x, y) - T{2} * std::sqrt(T(G) * hU(x, y));
	else
		SD = std::min(vD(x, y) - std::sqrt(T(G) * hD(x, y)), vstar - std::sqrt(T(G) * hstar));

	if (hU(x, y) <= T(DRY))
		SU = vD(x, y) + T{2} * std::sqrt(T(G) * hD(x, y));
	else
		SU = std::max(vU(x, y) + std::sqrt(T(G) * hU(x, y)), vstar + std::sqrt(T(G) * hstar));

	glm::tvec3<T, glm::precision::highp> GstarD{}, GstarU{};

	T Sstar = (SD * hU(x, y) * (vU(x, y) - SU) - SU * hD(x, y) * (vD(x, y) - SD)) /
	          (hU(x, y) * (vU(x, y) - SU) - hD(x, y) * (vD(x, y) - SD));
	Sstar = Sstar != Sstar ? T{0} : Sstar; // fix division by zero
	GstarD[0] = (SU * GD[0] - SD * GU[0] + SD * SU * (UU[0] - UD[0])) / (SU - SD);
	GstarD[2] = (SU * GD[2] - SD * GU[2] + SD * SU * (UU[2] - UD[2])) / (SU - SD);
	GstarD[0] = GstarD[0] != GstarD[0] ? T{0} : GstarD[0]; // fix division by zero
	GstarD[1] = GstarD[1] != GstarD[1] ? T{0} : GstarD[1]; // fix division by zero

	if (SD <= T{0} && T{0} <= Sstar) {
		GstarD[1] = uD(x, y) * GstarD[0];
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		GstarD[1] = uU(x, y) * GstarD[0];
	}

	GstarU[0] = GstarD[0];
	GstarU[2] = GstarD[2];
	if (SD <= T{0} && T{0} <= Sstar) {
		GstarU[1] = uD(x, y) * GstarU[0];
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		GstarU[1] = uU(x, y) * GstarU[0];
	}

	// assign Fe according to Riemann solution structure
	if (T{0} <= SD) {
		return GD;
	}
	else if (SD <= T{0} && T{0} <= Sstar) {
		return GstarD;
	}
	else if (Sstar <= T{0} && T{0} <= SU) {
		return GstarU;
	}
	else {
		return GU;
	}
}

template <typename T>
void calculations_parallel<T>::updatePrimitives(state<T> &state, const util::ghost_array<T> &dem,
                                                util::ghost_array<T> &height_carry, int x, int y)
{
	// preliminary update of state variables

	auto h = state.depth(x, y);
	if (h <= T(DRY)) {

		// if cell dry, velocities and fluxes are zero
		state.x_velocity(x, y) = T{0};
		state.y_velocity(x, y) = T{0};
		state.x_flow(x, y) = T{0};
		state.y_flow(x, y) = T{0};

		// if h was negative, find neighbor with the most water and subtract the negative h from it
		// to ensure mass conservation.
		// this is done only if after subtraction that neighbor has a depth > 1e-6 m. otherwise,
		// negative water depth carried onto next
		// iteration until suitable neighbor is identified or the cell is sufficiently recharged

		if (h < T{0}) {
			// spdlog::get("console")->warn("hcarry");
			height_carry(x, y) += h;

			// find the right neighbor
			int ip{}, jp{};
			std::vector<T> neighh{state.depth(x + 1, y), state.depth(x - 1, y),
			                      state.depth(x, y - 1), state.depth(x, y + 1)};

			// if multiple neighbors have same depth, chose a random one
			std::vector<int> neighhpos{};
			T nh_max_value = neighh[0];
			neighhpos.push_back(0);
			for (int i = 1; i < 4; ++i) {
				if (neighh[i] > nh_max_value) {
					neighhpos.clear();
					neighhpos.push_back(i);
					nh_max_value = neighh[i];
				}
				else if (neighh[i] == nh_max_value) {
					neighhpos.push_back(i);
				}
			}
			auto nhmax = neighhpos[std::rand() % neighhpos.size()];

			switch (nhmax) {
			case 0:
				jp = 0;
				ip = 1;
				break;
			case 1:
				jp = 0;
				ip = -1;
				break;
			case 2:
				jp = -1;
				ip = 0;
				break;
			case 3:
				jp = 1;
				ip = 0;
				break;
			}

			// if neighbor has enough water, take the negative water depth from it
			if ((state.depth(x + ip, y + jp) + height_carry(x, y)) > T(HCARRY_THRESHOLD)) {

				// adjust qx and qy to keep u and v unmodified
				state.x_flow(x + ip, y + jp) = state.x_flow(x + ip, y + jp) *
				                               (state.depth(x + ip, y + jp) + height_carry(x, y)) /
				                               state.depth(x + ip, y + jp);
				state.y_flow(x + ip, y + jp) = state.y_flow(x + ip, y + jp) *
				                               (state.depth(x + ip, y + jp) + height_carry(x, y)) /
				                               state.depth(x + ip, y + jp);

				// remove water and reset hcarry
				state.depth(x + ip, y + jp) += height_carry(x, y); // hcarry is negative
				height_carry(x, y) = T{0};
			}
		}

		// "fill" negative depth
		state.depth(x, y) = T{0};
		state.height(x, y) = dem(x, y);
	}
	else {
		state.x_velocity(x, y) = state.x_flow(x, y) / state.depth(x, y);
		state.y_velocity(x, y) = state.y_flow(x, y) / state.depth(x, y);
	}
}

} // namespace hn
