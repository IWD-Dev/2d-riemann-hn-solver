#include "stdafx.h"

#if defined _MSC_VER
#include <direct.h> // for creating dirs (only windows)
#elif defined __GNUC__
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include "hn/config.h"

#include "io/io_util.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class config<float_t>;
template class config<double_t>;

template <typename T> bool config<T>::load_from_json(const std::string &path)
{
	if (!io::util::file_exists(path)) return false;

	std::string config_str = io::util::read_file_as_text(path);
	if (config_str.empty()) return false;

	nlohmann::json js;
	try {
		js = js.parse(config_str);
		this->json = js;
	}
	catch (const std::exception &ex) {
		spdlog::get("console")->error("Error parsing json config file: " + std::string(ex.what()));
		return false;
	}

	try {
		// simulation
		time_stop = js["time_stop"].get<T>();
		time_step_AIMD_increase = js["time_step_AIMD_increase"].get<T>();
		time_step_AIMD_penalty = js["time_step_AIMD_penalty"].get<T>();
		time_step_AIMD_bonus = js["time_step_AIMD_bonus"].get<T>();
		time_step_min = js["time_step_min"].get<T>();
		time_step_max = js["time_step_max"].get<T>();
		time_sim_out_file = js["time_sim_out_file"].get<T>();
		time_sim_console_log = js["time_sim_console_log"].get<T>();
		time_rollback = js["time_rollback"].get<T>();
		rollback_count = js["rollback_count"].get<size_t>();
		rollback_strikes = js["rollback_strikes"].get<size_t>();
		courant_tolerance_high = js["courant_tolerance_high"].get<T>();
		courant_tolerance_low = js["courant_tolerance_low"].get<T>();

		// in
		in_dem = js["in_dem"].get<std::string>();
		in_eta = js["in_eta"].get<std::string>();
		in_x_velocity = js["in_x_velocity"].get<std::string>();
		in_y_velocity = js["in_y_velocity"].get<std::string>();
		in_manning = js["in_manning"].get<std::string>();

		// out
		result_prefix = js["result_prefix"].get<std::string>();
		out_path = js["out_path"].get<std::string>();
		out_depth = js["out_depth"].get<bool>();
		out_height = js["out_height"].get<bool>();
		out_x_velocity = js["out_x_velocity"].get<bool>();
		out_y_velocity = js["out_y_velocity"].get<bool>();
		out_vmag = js["out_vmag"].get<bool>();
		out_flowd = js["out_flowd"].get<bool>();

		// hydrographs
		auto hgs = js["hydrographs"];
		for (auto it = hgs.begin(); it != hgs.end(); ++it) {
			auto hgj = *it;
			hydrograph<T> hg{};

			hg.name = hgj["name"].get<std::string>();

			auto steps = hgj["steps"];
			for (auto it2 = steps.begin(); it2 != steps.end(); ++it2) {
				auto step = *it2;

				T time = step["time"].get<T>();
				T val = step["value"].get<T>();
				std::pair<T, T> ts = std::make_pair(time, val);
				hg.steps.push_back(ts);
			}
			hg.steps.shrink_to_fit();
			hydrographs.push_back(hg);
		}

		// boundary conditions
		auto bcs = js["boundary_conditions"];
		for (auto it = bcs.begin(); it != bcs.end(); ++it) {
			auto condition = *it;
			boundary::boundary_condition<T> bc{};

			bc.start = condition["start"].get<T>();
			bc.end = condition["end"].get<T>();
			bc.edge = boundary::str_to_edge(condition["edge"].get<std::string>());
			bc.type = boundary::str_to_type(condition["type"].get<std::string>());
			bc.hydrograph = condition["hydrograph"].get<std::string>();

			boundary_conditions.push_back(bc);
		}

		// create out directory if it not exists
		// https://stackoverflow.com/questions/20358455/cross-platform-way-to-make-a-directory

		// TODO: create whole path, e.g. abc/xyz/
		// at the moment only abc/ is possible or
		// abc/ is already created then abc/xyz/ is possible

#if defined _MSC_VER
		int err = _mkdir(out_path.c_str());
		if (err != 0) {        // something went wrong
			if (errno != 17) { // 17 directory exists already, which is ok
				spdlog::get("console")->error("Output directory can not be created: " + out_path +
				                              ". Error: " + std::string(strerror(errno)));
				return false;
			}
		}
#elif defined __GNUC__
		int err = _mkdir(out_path.c_str());
		if (err != 0) {        // something went wrong
			if (errno != 17) { // 17 directory exists already, which is ok
				spdlog::get("console")->error("Output directory can not be created: " + out_path +
				                              ". Error: " + std::string(strerror(errno)));
				return false;
			}
		}
#endif
	}
	catch (const std::exception &ex) {
		spdlog::get("console")->error("Error read json as config: " + std::string(ex.what()));
		return false;
	}

	return true;
}
template <typename T> std::string config<T>::to_string() { return json.dump(4); }
} // namespace hn
