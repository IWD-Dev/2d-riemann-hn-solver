#include "stdafx.h"

#include "hn/reconstruction.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class reconstruction<float_t>;
template class reconstruction<double_t>;
template class reconstruction_parallel<float_t>;
template class reconstruction_parallel<double_t>;

template <typename T>
void reconstruction<T>::fill_1dry(const util::ghost_array<T> &eta, const util::ghost_array<T> &h,
                                  const util::ghost_array<T> &u, const util::ghost_array<T> &v,
                                  const util::ghost_array<T> &qx, const util::ghost_array<T> &qy,
                                  const util::ghost_array<T> &zb, const int x, const int y)
{
	etaLe = eta(x, y);
	etaRw = eta(x, y);
	etaDn = eta(x, y);
	etaUs = eta(x, y);
	etaRe = eta(x + 1, y);
	etaLw = eta(x - 1, y);
	etaUn = eta(x, y - 1);
	etaDs = eta(x, y + 1);

	hLe = h(x, y);
	hRw = h(x, y);
	hDn = h(x, y);
	hUs = h(x, y);
	hRe = h(x + 1, y);
	hLw = h(x - 1, y);
	hUn = h(x, y - 1);
	hDs = h(x, y + 1);

	qxLe = T{0};
	qxRw = T{0};
	qxDn = T{0};
	qxUs = T{0};
	qxRe = qx(x + 1, y);
	qxLw = qx(x - 1, y);
	qxUn = qx(x, y - 1);
	qxDs = qx(x, y + 1);

	qyLe = T{0};
	qyRw = T{0};
	qyDn = T{0};
	qyUs = T{0};
	qyRe = qy(x + 1, y);
	qyLw = qy(x - 1, y);
	qyUn = qy(x, y - 1);
	qyDs = qy(x, y + 1);

	uLe = T{0};
	uRw = T{0};
	uDn = T{0};
	uUs = T{0};
	uRe = u(x + 1, y);
	uLw = u(x - 1, y);
	uUn = u(x, y - 1);
	uDs = u(x, y + 1);

	vLe = T{0};
	vRw = T{0};
	vDn = T{0};
	vUs = T{0};
	vRe = v(x + 1, y);
	vLw = v(x - 1, y);
	vUn = v(x, y - 1);
	vDs = v(x, y + 1);

	zbLe = zb(x, y);
	zbRw = zb(x, y);
	zbDn = zb(x, y);
	zbUs = zb(x, y);
	zbRe = zb(x + 1, y);
	zbLw = zb(x - 1, y);
	zbUn = zb(x, y - 1);
	zbDs = zb(x, y + 1);
}
template <typename T>
void reconstruction<T>::fill_1drywet(const util::ghost_array<T> &eta, const util::ghost_array<T> &h,
                                     const util::ghost_array<T> &u, const util::ghost_array<T> &v,
                                     const util::ghost_array<T> &qx, const util::ghost_array<T> &qy,
                                     const util::ghost_array<T> &zb, const int x, const int y)
{
	etaLe = eta(x, y);
	etaRw = eta(x, y);
	etaDn = eta(x, y);
	etaUs = eta(x, y);
	etaRe = eta(x + 1, y);
	etaLw = eta(x - 1, y);
	etaUn = eta(x, y - 1);
	etaDs = eta(x, y + 1);

	hLe = h(x, y);
	hRw = h(x, y);
	hDn = h(x, y);
	hUs = h(x, y);
	hRe = h(x + 1, y);
	hLw = h(x - 1, y);
	hUn = h(x, y - 1);
	hDs = h(x, y + 1);

	qxLe = qx(x, y);
	qxRw = qx(x, y);
	qxDn = qx(x, y);
	qxUs = qx(x, y);
	qxRe = qx(x + 1, y);
	qxLw = qx(x - 1, y);
	qxUn = qx(x, y - 1);
	qxDs = qx(x, y + 1);

	qyLe = qy(x, y);
	qyRw = qy(x, y);
	qyDn = qy(x, y);
	qyUs = qy(x, y);
	qyRe = qy(x + 1, y);
	qyLw = qy(x - 1, y);
	qyUn = qy(x, y - 1);
	qyDs = qy(x, y + 1);

	uLe = u(x, y);
	uRw = u(x, y);
	uDn = u(x, y);
	uUs = u(x, y);
	uRe = u(x + 1, y);
	uLw = u(x - 1, y);
	uUn = u(x, y - 1);
	uDs = u(x, y + 1);

	vLe = v(x, y);
	vRw = v(x, y);
	vDn = v(x, y);
	vUs = v(x, y);
	vRe = v(x + 1, y);
	vLw = v(x - 1, y);
	vUn = v(x, y - 1);
	vDs = v(x, y + 1);

	zbLe = zb(x, y);
	zbRw = zb(x, y);
	zbDn = zb(x, y);
	zbUs = zb(x, y);
	zbRe = zb(x + 1, y);
	zbLw = zb(x - 1, y);
	zbUn = zb(x, y - 1);
	zbDs = zb(x, y + 1);
}
template <typename T>
void reconstruction<T>::fill_2inn(const dtype type, const util::ghost_array<T> &arr, const T dx,
                                  const T dy, const int x, const int y)
{
	T val = arr(x, y);

	// x-direction
	T inn1_x = (val - arr(x - 1, y)) / dx;
	T inn2_x = (arr(x + 1, y) - val) / dx;
	T limslope_x = minmod(inn1_x, inn2_x);

	// y-direction
	T inn1_y = (val - arr(x, y + 1)) / dy;
	T inn2_y = (arr(x, y - 1) - val) / dy;
	T limslope_y = minmod(inn1_y, inn2_y);

	switch (type) {
	case dtype::eta:
		etaLe = val + T(0.5) * dx * limslope_x;
		etaRw = val - T(0.5) * dx * limslope_x;
		etaDn = val + T(0.5) * dy * limslope_y;
		etaUs = val - T(0.5) * dy * limslope_y;
		break;
	case dtype::h:
		hLe = val + T(0.5) * dx * limslope_x;
		hRw = val - T(0.5) * dx * limslope_x;
		hDn = val + T(0.5) * dy * limslope_y;
		hUs = val - T(0.5) * dy * limslope_y;
		break;
	case dtype::qx:
		qxLe = val + T(0.5) * dx * limslope_x;
		qxRw = val - T(0.5) * dx * limslope_x;
		qxDn = val + T(0.5) * dy * limslope_y;
		qxUs = val - T(0.5) * dy * limslope_y;
		break;
	case dtype::qy:
		qyLe = val + T(0.5) * dx * limslope_x;
		qyRw = val - T(0.5) * dx * limslope_x;
		qyDn = val + T(0.5) * dy * limslope_y;
		qyUs = val - T(0.5) * dy * limslope_y;
		break;
	default:
		spdlog::get("console")->warn(
		    "Unsupported dtype given in 2nd Order inner reconstruction! Will do nothing!");
	}
}
template <typename T>
void reconstruction<T>::fill_2out(const dtype type, const util::ghost_array<T> &arr, const T dx,
                                  const T dy, const int x, const int y, const int ncols,
                                  const int nrows)
{
	// to assure safe access arr.at() is used instead of arr() or arr.f_at()

	// TODO: preload even more values
	T val = arr(x, y);

	// x-direction
	T out1_e = (arr.at(x + 2, y) - arr(x + 1, y)) / dx;
	T out2_e = (arr(x + 1, y) - val) / dx;
	T limslope_e = minmod(out1_e, out2_e);

	T out1_w = (arr(x - 1, y) - arr.at(x - 2, y)) / dx;
	T out2_w = (val - arr(x - 1, y)) / dx;
	T limslope_w = minmod(out1_w, out2_w);

	// y-direction
	T out1_n = (arr.at(x, y - 2) - arr(x, y - 1)) / dy;
	T out2_n = (arr(x, y - 1) - val) / dy;
	T limslope_n = minmod(out1_n, out2_n);

	T out1_s = (arr(x, y + 1) - arr.at(x, y + 2)) / dy;
	T out2_s = (val - arr(x, y + 1)) / dy;
	T limslope_s = minmod(out1_s, out2_s);

	switch (type) {
	case dtype::eta:
		etaRe = x == (ncols - 1) ? arr(x + 1, y) : arr(x + 1, y) - T(0.5) * dx * limslope_e;
		etaLw = x == 0 ? arr(x - 1, y) : arr(x - 1, y) + T(0.5) * dx * limslope_w;
		etaUn = y == 0 ? arr(x, y - 1) : arr(x, y - 1) - T(0.5) * dy * limslope_n;
		etaDs = y == (nrows - 1) ? arr(x, y + 1) : arr(x, y + 1) + T(0.5) * dy * limslope_s;
		break;
	case dtype::h:
		hRe = x == (ncols - 1) ? arr(x + 1, y) : arr(x + 1, y) - T(0.5) * dx * limslope_e;
		hLw = x == 0 ? arr(x - 1, y) : arr(x - 1, y) + T(0.5) * dx * limslope_w;
		hUn = y == 0 ? arr(x, y - 1) : arr(x, y - 1) - T(0.5) * dy * limslope_n;
		hDs = y == (nrows - 1) ? arr(x, y + 1) : arr(x, y + 1) + T(0.5) * dy * limslope_s;
		break;
	case dtype::qx:
		qxRe = x == (ncols - 1) ? arr(x + 1, y) : arr(x + 1, y) - T(0.5) * dx * limslope_e;
		qxLw = x == 0 ? arr(x - 1, y) : arr(x - 1, y) + T(0.5) * dx * limslope_w;
		qxUn = y == 0 ? arr(x, y - 1) : arr(x, y - 1) - T(0.5) * dy * limslope_n;
		qxDs = y == (nrows - 1) ? arr(x, y + 1) : arr(x, y + 1) + T(0.5) * dy * limslope_s;
		break;
	case dtype::qy:
		qyRe = x == (ncols - 1) ? arr(x + 1, y) : arr(x + 1, y) - T(0.5) * dx * limslope_e;
		qyLw = x == 0 ? arr(x - 1, y) : arr(x - 1, y) + T(0.5) * dx * limslope_w;
		qyUn = y == 0 ? arr(x, y - 1) : arr(x, y - 1) - T(0.5) * dy * limslope_n;
		qyDs = y == (nrows - 1) ? arr(x, y + 1) : arr(x, y + 1) + T(0.5) * dy * limslope_s;
		break;
	default:
		spdlog::get("console")->warn(
		    "Unsupported dtype given in 2nd Order outer reconstruction! Will do nothing!");
	}
}
template <typename T> void reconstruction<T>::recalculate()
{
	// bottom elevation
	zbLe = etaLe - hLe;
	zbRw = etaRw - hRw;
	zbDn = etaDn - hDn;
	zbUs = etaUs - hUs;
	zbRe = etaRe - hRe;
	zbLw = etaLw - hLw;
	zbUn = etaUn - hUn;
	zbDs = etaDs - hDs;

	// velocities
	uLe = qxLe / hLe;
	uRw = qxRw / hRw;
	uDn = qxDn / hDn;
	uUs = qxUs / hUs;
	uRe = qxRe / hRe;
	uLw = qxLw / hLw;
	uUn = qxUn / hUn;
	uDs = qxDs / hDs;

	vLe = qyLe / hLe;
	vRw = qyRw / hRw;
	vDn = qyDn / hDn;
	vUs = qyUs / hUs;
	vRe = qyRe / hRe;
	vLw = qyLw / hLw;
	vUn = qyUn / hUn;
	vDs = qyDs / hDs;
}
template <typename T> T reconstruction<T>::minmod(T val1, T val2)
{
	// ((inn1_y * inn2_y) < T{ 0 }) ? T{ 0 } : std::fabs(inn1_y) < std::fabs(inn2_y) ?
	// inn1_y : inn2_y;

	if (val1 * val2 < T{0})
		return T{0};
	else if (std::fabs(val1) < std::fabs(val2))
		return val1;
	else
		return val2;
}

template <typename T> reconstruction_parallel<T>::reconstruction_parallel(int ncols, int nrows)
{
	etaLe = {ncols, nrows};
	etaRe = {ncols, nrows};
	etaLw = {ncols, nrows};
	etaRw = {ncols, nrows};
	etaDn = {ncols, nrows};
	etaUn = {ncols, nrows};
	etaDs = {ncols, nrows};
	etaUs = {ncols, nrows};
	hLe = {ncols, nrows};
	hRe = {ncols, nrows};
	hLw = {ncols, nrows};
	hRw = {ncols, nrows};
	hDn = {ncols, nrows};
	hUn = {ncols, nrows};
	hDs = {ncols, nrows};
	hUs = {ncols, nrows};
	uLe = {ncols, nrows};
	uRe = {ncols, nrows};
	uLw = {ncols, nrows};
	uRw = {ncols, nrows};
	uDn = {ncols, nrows};
	uUn = {ncols, nrows};
	uDs = {ncols, nrows};
	uUs = {ncols, nrows};
	vLe = {ncols, nrows};
	vRe = {ncols, nrows};
	vLw = {ncols, nrows};
	vRw = {ncols, nrows};
	vDn = {ncols, nrows};
	vUn = {ncols, nrows};
	vDs = {ncols, nrows};
	vUs = {ncols, nrows};
	qxLe = {ncols, nrows};
	qxRe = {ncols, nrows};
	qxLw = {ncols, nrows};
	qxRw = {ncols, nrows};
	qxDn = {ncols, nrows};
	qxUn = {ncols, nrows};
	qxDs = {ncols, nrows};
	qxUs = {ncols, nrows};
	qyLe = {ncols, nrows};
	qyRe = {ncols, nrows};
	qyLw = {ncols, nrows};
	qyRw = {ncols, nrows};
	qyDn = {ncols, nrows};
	qyUn = {ncols, nrows};
	qyDs = {ncols, nrows};
	qyUs = {ncols, nrows};
	zbLe = {ncols, nrows};
	zbRe = {ncols, nrows};
	zbLw = {ncols, nrows};
	zbRw = {ncols, nrows};
	zbDn = {ncols, nrows};
	zbUn = {ncols, nrows};
	zbDs = {ncols, nrows};
	zbUs = {ncols, nrows};
}

/////////////////////////////////////////// dry ///////////////////////////////////////////////

template <typename T>
void reconstruction_parallel<T>::fill_1dry_eta(const util::ghost_array<T> &eta, const int x,
                                               const int y)
{
	etaLe(x, y) = eta(x, y);
	etaRw(x, y) = eta(x, y);
	etaDn(x, y) = eta(x, y);
	etaUs(x, y) = eta(x, y);
	etaRe(x, y) = eta(x + 1, y);
	etaLw(x, y) = eta(x - 1, y);
	etaUn(x, y) = eta(x, y - 1);
	etaDs(x, y) = eta(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_h(const util::ghost_array<T> &h, const int x,
                                             const int y)
{
	hLe(x, y) = h(x, y);
	hRw(x, y) = h(x, y);
	hDn(x, y) = h(x, y);
	hUs(x, y) = h(x, y);
	hRe(x, y) = h(x + 1, y);
	hLw(x, y) = h(x - 1, y);
	hUn(x, y) = h(x, y - 1);
	hDs(x, y) = h(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_u(const util::ghost_array<T> &u, const int x,
                                             const int y)
{
	uLe(x, y) = T{0};
	uRw(x, y) = T{0};
	uDn(x, y) = T{0};
	uUs(x, y) = T{0};
	uRe(x, y) = u(x + 1, y);
	uLw(x, y) = u(x - 1, y);
	uUn(x, y) = u(x, y - 1);
	uDs(x, y) = u(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_v(const util::ghost_array<T> &v, const int x,
                                             const int y)
{
	vLe(x, y) = T{0};
	vRw(x, y) = T{0};
	vDn(x, y) = T{0};
	vUs(x, y) = T{0};
	vRe(x, y) = v(x + 1, y);
	vLw(x, y) = v(x - 1, y);
	vUn(x, y) = v(x, y - 1);
	vDs(x, y) = v(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_qx(const util::ghost_array<T> &qx, const int x,
                                              const int y)
{
	qxLe(x, y) = T{0};
	qxRw(x, y) = T{0};
	qxDn(x, y) = T{0};
	qxUs(x, y) = T{0};
	qxRe(x, y) = qx(x + 1, y);
	qxLw(x, y) = qx(x - 1, y);
	qxUn(x, y) = qx(x, y - 1);
	qxDs(x, y) = qx(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_qy(const util::ghost_array<T> &qy, const int x,
                                              const int y)
{
	qyLe(x, y) = T{0};
	qyRw(x, y) = T{0};
	qyDn(x, y) = T{0};
	qyUs(x, y) = T{0};
	qyRe(x, y) = qy(x + 1, y);
	qyLw(x, y) = qy(x - 1, y);
	qyUn(x, y) = qy(x, y - 1);
	qyDs(x, y) = qy(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1dry_zb(const util::ghost_array<T> &zb, const int x,
                                              const int y)
{
	zbLe(x, y) = zb(x, y);
	zbRw(x, y) = zb(x, y);
	zbDn(x, y) = zb(x, y);
	zbUs(x, y) = zb(x, y);
	zbRe(x, y) = zb(x + 1, y);
	zbLw(x, y) = zb(x - 1, y);
	zbUn(x, y) = zb(x, y - 1);
	zbDs(x, y) = zb(x, y + 1);
}

/////////////////////////////////////////// drywet ////////////////////////////////////////////

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_eta(const util::ghost_array<T> &eta, const int x,
                                                  const int y)
{
	etaLe(x, y) = eta(x, y);
	etaRw(x, y) = eta(x, y);
	etaDn(x, y) = eta(x, y);
	etaUs(x, y) = eta(x, y);
	etaRe(x, y) = eta(x + 1, y);
	etaLw(x, y) = eta(x - 1, y);
	etaUn(x, y) = eta(x, y - 1);
	etaDs(x, y) = eta(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_h(const util::ghost_array<T> &h, const int x,
                                                const int y)
{
	hLe(x, y) = h(x, y);
	hRw(x, y) = h(x, y);
	hDn(x, y) = h(x, y);
	hUs(x, y) = h(x, y);
	hRe(x, y) = h(x + 1, y);
	hLw(x, y) = h(x - 1, y);
	hUn(x, y) = h(x, y - 1);
	hDs(x, y) = h(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_u(const util::ghost_array<T> &u, const int x,
                                                const int y)
{
	uLe(x, y) = u(x, y);
	uRw(x, y) = u(x, y);
	uDn(x, y) = u(x, y);
	uUs(x, y) = u(x, y);
	uRe(x, y) = u(x + 1, y);
	uLw(x, y) = u(x - 1, y);
	uUn(x, y) = u(x, y - 1);
	uDs(x, y) = u(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_v(const util::ghost_array<T> &v, const int x,
                                                const int y)
{
	vLe(x, y) = v(x, y);
	vRw(x, y) = v(x, y);
	vDn(x, y) = v(x, y);
	vUs(x, y) = v(x, y);
	vRe(x, y) = v(x + 1, y);
	vLw(x, y) = v(x - 1, y);
	vUn(x, y) = v(x, y - 1);
	vDs(x, y) = v(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_qx(const util::ghost_array<T> &qx, const int x,
                                                 const int y)
{
	qxLe(x, y) = qx(x, y);
	qxRw(x, y) = qx(x, y);
	qxDn(x, y) = qx(x, y);
	qxUs(x, y) = qx(x, y);
	qxRe(x, y) = qx(x + 1, y);
	qxLw(x, y) = qx(x - 1, y);
	qxUn(x, y) = qx(x, y - 1);
	qxDs(x, y) = qx(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_qy(const util::ghost_array<T> &qy, const int x,
                                                 const int y)
{
	qyLe(x, y) = qy(x, y);
	qyRw(x, y) = qy(x, y);
	qyDn(x, y) = qy(x, y);
	qyUs(x, y) = qy(x, y);
	qyRe(x, y) = qy(x + 1, y);
	qyLw(x, y) = qy(x - 1, y);
	qyUn(x, y) = qy(x, y - 1);
	qyDs(x, y) = qy(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_1drywet_zb(const util::ghost_array<T> &zb, const int x,
                                                 const int y)
{
	zbLe(x, y) = zb(x, y);
	zbRw(x, y) = zb(x, y);
	zbDn(x, y) = zb(x, y);
	zbUs(x, y) = zb(x, y);
	zbRe(x, y) = zb(x + 1, y);
	zbLw(x, y) = zb(x - 1, y);
	zbUn(x, y) = zb(x, y - 1);
	zbDs(x, y) = zb(x, y + 1);
}

template <typename T>
void reconstruction_parallel<T>::fill_2inn_eta(const util::ghost_array<T> &eta, const T dx,
                                               const T dy, const int x, const int y)
{
	T val = eta(x, y);
	// x-direction
	T inn1_x = (val - eta(x - 1, y)) / dx;
	T inn2_x = (eta(x + 1, y) - val) / dx;
	T limslope_x = minmod(inn1_x, inn2_x);

	// y-direction
	T inn1_y = (val - eta(x, y + 1)) / dy;
	T inn2_y = (eta(x, y - 1) - val) / dy;
	T limslope_y = minmod(inn1_y, inn2_y);

	etaLe(x, y) = val + T(0.5) * dx * limslope_x;
	etaRw(x, y) = val - T(0.5) * dx * limslope_x;
	etaDn(x, y) = val + T(0.5) * dy * limslope_y;
	etaUs(x, y) = val - T(0.5) * dy * limslope_y;
}

template <typename T>
void reconstruction_parallel<T>::fill_2inn_h(const util::ghost_array<T> &h, const T dx, const T dy,
                                             const int x, const int y)
{
	T val = h(x, y);
	// x-direction
	T inn1_x = (val - h(x - 1, y)) / dx;
	T inn2_x = (h(x + 1, y) - val) / dx;
	T limslope_x = minmod(inn1_x, inn2_x);

	// y-direction
	T inn1_y = (val - h(x, y + 1)) / dy;
	T inn2_y = (h(x, y - 1) - val) / dy;
	T limslope_y = minmod(inn1_y, inn2_y);

	hLe(x, y) = val + T(0.5) * dx * limslope_x;
	hRw(x, y) = val - T(0.5) * dx * limslope_x;
	hDn(x, y) = val + T(0.5) * dy * limslope_y;
	hUs(x, y) = val - T(0.5) * dy * limslope_y;
}

template <typename T>
void reconstruction_parallel<T>::fill_2inn_qx(const util::ghost_array<T> &qx, const T dx,
                                              const T dy, const int x, const int y)
{
	T val = qx(x, y);
	// x-direction
	T inn1_x = (val - qx(x - 1, y)) / dx;
	T inn2_x = (qx(x + 1, y) - val) / dx;
	T limslope_x = minmod(inn1_x, inn2_x);

	// y-direction
	T inn1_y = (val - qx(x, y + 1)) / dy;
	T inn2_y = (qx(x, y - 1) - val) / dy;
	T limslope_y = minmod(inn1_y, inn2_y);

	qxLe(x, y) = val + T(0.5) * dx * limslope_x;
	qxRw(x, y) = val - T(0.5) * dx * limslope_x;
	qxDn(x, y) = val + T(0.5) * dy * limslope_y;
	qxUs(x, y) = val - T(0.5) * dy * limslope_y;
}

template <typename T>
void reconstruction_parallel<T>::fill_2inn_qy(const util::ghost_array<T> &qy, const T dx,
                                              const T dy, const int x, const int y)
{
	T val = qy(x, y);
	// x-direction
	T inn1_x = (val - qy(x - 1, y)) / dx;
	T inn2_x = (qy(x + 1, y) - val) / dx;
	T limslope_x = minmod(inn1_x, inn2_x);

	// y-direction
	T inn1_y = (val - qy(x, y + 1)) / dy;
	T inn2_y = (qy(x, y - 1) - val) / dy;
	T limslope_y = minmod(inn1_y, inn2_y);

	qyLe(x, y) = val + T(0.5) * dx * limslope_x;
	qyRw(x, y) = val - T(0.5) * dx * limslope_x;
	qyDn(x, y) = val + T(0.5) * dy * limslope_y;
	qyUs(x, y) = val - T(0.5) * dy * limslope_y;
}

template <typename T>
void reconstruction_parallel<T>::fill_2out_eta(const util::ghost_array<T> &eta, const T dx,
                                               const T dy, const int x, const int y,
                                               const int ncols, const int nrows)
{
	T val = eta(x, y);
	T valxp1 = eta(x + 1, y);
	T valxm1 = eta(x - 1, y);
	T valyp1 = eta(x, y + 1);
	T valym1 = eta(x, y - 1);

	// x-direction
	T out1_e = (eta.at(x + 2, y) - valxp1) / dx;
	T out2_e = (valxp1 - val) / dx;
	T limslope_e = minmod(out1_e, out2_e);

	T out1_w = (valxm1 - eta.at(x - 2, y)) / dx;
	T out2_w = (val - valxm1) / dx;
	T limslope_w = minmod(out1_w, out2_w);

	// y-direction
	T out1_n = (eta.at(x, y - 2) - valym1) / dy;
	T out2_n = (valym1 - val) / dy;
	T limslope_n = minmod(out1_n, out2_n);

	T out1_s = (valyp1 - eta.at(x, y + 2)) / dy;
	T out2_s = (val - valyp1) / dy;
	T limslope_s = minmod(out1_s, out2_s);

	etaRe(x, y) = x == (ncols - 1) ? eta(x + 1, y) : eta(x + 1, y) - T(0.5) * dx * limslope_e;
	etaLw(x, y) = x == 0 ? eta(x - 1, y) : eta(x - 1, y) + T(0.5) * dx * limslope_w;
	etaUn(x, y) = y == 0 ? eta(x, y - 1) : eta(x, y - 1) - T(0.5) * dy * limslope_n;
	etaDs(x, y) = y == (nrows - 1) ? eta(x, y + 1) : eta(x, y + 1) + T(0.5) * dy * limslope_s;
}

template <typename T>
void reconstruction_parallel<T>::fill_2out_h(const util::ghost_array<T> &h, const T dx, const T dy,
                                             const int x, const int y, const int ncols,
                                             const int nrows)
{
	T val = h(x, y);
	T valxp1 = h(x + 1, y);
	T valxm1 = h(x - 1, y);
	T valyp1 = h(x, y + 1);
	T valym1 = h(x, y - 1);

	// x-direction
	T out1_e = (h.at(x + 2, y) - valxp1) / dx;
	T out2_e = (valxp1 - val) / dx;
	T limslope_e = minmod(out1_e, out2_e);

	T out1_w = (valxm1 - h.at(x - 2, y)) / dx;
	T out2_w = (val - valxm1) / dx;
	T limslope_w = minmod(out1_w, out2_w);

	// y-direction
	T out1_n = (h.at(x, y - 2) - valym1) / dy;
	T out2_n = (valym1 - val) / dy;
	T limslope_n = minmod(out1_n, out2_n);

	T out1_s = (valyp1 - h.at(x, y + 2)) / dy;
	T out2_s = (val - valyp1) / dy;
	T limslope_s = minmod(out1_s, out2_s);

	hRe(x, y) = x == (ncols - 1) ? h(x + 1, y) : h(x + 1, y) - T(0.5) * dx * limslope_e;
	hLw(x, y) = x == 0 ? h(x - 1, y) : h(x - 1, y) + T(0.5) * dx * limslope_w;
	hUn(x, y) = y == 0 ? h(x, y - 1) : h(x, y - 1) - T(0.5) * dy * limslope_n;
	hDs(x, y) = y == (nrows - 1) ? h(x, y + 1) : h(x, y + 1) + T(0.5) * dy * limslope_s;
}

template <typename T>
void reconstruction_parallel<T>::fill_2out_qx(const util::ghost_array<T> &qx, const T dx,
                                              const T dy, const int x, const int y, const int ncols,
                                              const int nrows)
{
	T val = qx(x, y);
	T valxp1 = qx(x + 1, y);
	T valxm1 = qx(x - 1, y);
	T valyp1 = qx(x, y + 1);
	T valym1 = qx(x, y - 1);

	// x-direction
	T out1_e = (qx.at(x + 2, y) - valxp1) / dx;
	T out2_e = (valxp1 - val) / dx;
	T limslope_e = minmod(out1_e, out2_e);

	T out1_w = (valxm1 - qx.at(x - 2, y)) / dx;
	T out2_w = (val - valxm1) / dx;
	T limslope_w = minmod(out1_w, out2_w);

	// y-direction
	T out1_n = (qx.at(x, y - 2) - valym1) / dy;
	T out2_n = (valym1 - val) / dy;
	T limslope_n = minmod(out1_n, out2_n);

	T out1_s = (valyp1 - qx.at(x, y + 2)) / dy;
	T out2_s = (val - valyp1) / dy;
	T limslope_s = minmod(out1_s, out2_s);

	qxRe(x, y) = x == (ncols - 1) ? qx(x + 1, y) : qx(x + 1, y) - T(0.5) * dx * limslope_e;
	qxLw(x, y) = x == 0 ? qx(x - 1, y) : qx(x - 1, y) + T(0.5) * dx * limslope_w;
	qxUn(x, y) = y == 0 ? qx(x, y - 1) : qx(x, y - 1) - T(0.5) * dy * limslope_n;
	qxDs(x, y) = y == (nrows - 1) ? qx(x, y + 1) : qx(x, y + 1) + T(0.5) * dy * limslope_s;
}

template <typename T>
void reconstruction_parallel<T>::fill_2out_qy(const util::ghost_array<T> &qy, const T dx,
                                              const T dy, const int x, const int y, const int ncols,
                                              const int nrows)
{
	T val = qy(x, y);
	T valxp1 = qy(x + 1, y);
	T valxm1 = qy(x - 1, y);
	T valyp1 = qy(x, y + 1);
	T valym1 = qy(x, y - 1);

	// x-direction
	T out1_e = (qy.at(x + 2, y) - valxp1) / dx;
	T out2_e = (valxp1 - val) / dx;
	T limslope_e = minmod(out1_e, out2_e);

	T out1_w = (valxm1 - qy.at(x - 2, y)) / dx;
	T out2_w = (val - valxm1) / dx;
	T limslope_w = minmod(out1_w, out2_w);

	// y-direction
	T out1_n = (qy.at(x, y - 2) - valym1) / dy;
	T out2_n = (valym1 - val) / dy;
	T limslope_n = minmod(out1_n, out2_n);

	T out1_s = (valyp1 - qy.at(x, y + 2)) / dy;
	T out2_s = (val - valyp1) / dy;
	T limslope_s = minmod(out1_s, out2_s);

	qyRe(x, y) = x == (ncols - 1) ? qy(x + 1, y) : qy(x + 1, y) - T(0.5) * dx * limslope_e;
	qyLw(x, y) = x == 0 ? qy(x - 1, y) : qy(x - 1, y) + T(0.5) * dx * limslope_w;
	qyUn(x, y) = y == 0 ? qy(x, y - 1) : qy(x, y - 1) - T(0.5) * dy * limslope_n;
	qyDs(x, y) = y == (nrows - 1) ? qy(x, y + 1) : qy(x, y + 1) + T(0.5) * dy * limslope_s;
}

template <typename T> void reconstruction_parallel<T>::recalculate_u(const int x, const int y)
{
	uLe(x, y) = qxLe(x, y) / hLe(x, y);
	uRw(x, y) = qxRw(x, y) / hRw(x, y);
	uDn(x, y) = qxDn(x, y) / hDn(x, y);
	uUs(x, y) = qxUs(x, y) / hUs(x, y);
	uRe(x, y) = qxRe(x, y) / hRe(x, y);
	uLw(x, y) = qxLw(x, y) / hLw(x, y);
	uUn(x, y) = qxUn(x, y) / hUn(x, y);
	uDs(x, y) = qxDs(x, y) / hDs(x, y);
}

template <typename T> void reconstruction_parallel<T>::recalculate_v(const int x, const int y)
{
	vLe(x, y) = qyLe(x, y) / hLe(x, y);
	vRw(x, y) = qyRw(x, y) / hRw(x, y);
	vDn(x, y) = qyDn(x, y) / hDn(x, y);
	vUs(x, y) = qyUs(x, y) / hUs(x, y);
	vRe(x, y) = qyRe(x, y) / hRe(x, y);
	vLw(x, y) = qyLw(x, y) / hLw(x, y);
	vUn(x, y) = qyUn(x, y) / hUn(x, y);
	vDs(x, y) = qyDs(x, y) / hDs(x, y);
}

template <typename T> void reconstruction_parallel<T>::recalculate_zb(const int x, const int y)
{
	zbLe(x, y) = etaLe(x, y) - hLe(x, y);
	zbRw(x, y) = etaRw(x, y) - hRw(x, y);
	zbDn(x, y) = etaDn(x, y) - hDn(x, y);
	zbUs(x, y) = etaUs(x, y) - hUs(x, y);
	zbRe(x, y) = etaRe(x, y) - hRe(x, y);
	zbLw(x, y) = etaLw(x, y) - hLw(x, y);
	zbUn(x, y) = etaUn(x, y) - hUn(x, y);
	zbDs(x, y) = etaDs(x, y) - hDs(x, y);
}

template <typename T> inline T reconstruction_parallel<T>::minmod(T val1, T val2)
{
	// ((inn1_y * inn2_y) < T{ 0 }) ? T{ 0 } : std::fabs(inn1_y) < std::fabs(inn2_y) ?
	// inn1_y : inn2_y;

	if (val1 * val2 < T{0})
		return T{0};
	else if (std::fabs(val1) < std::fabs(val2))
		return val1;
	else
		return val2;
}
}
