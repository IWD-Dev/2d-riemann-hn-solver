#include "stdafx.h"

#include "hn/hydrograph.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class hydrograph<float_t>;
template class hydrograph<double_t>;

template <typename T>
typename std::vector<hydrograph<T>>::const_iterator
hydrograph<T>::get_hydrograph_by_name(const std::string &name,
                                      const std::vector<hydrograph<T>> &hydrographs)
{
	for (auto it = hydrographs.begin(); it != hydrographs.end(); ++it) {
		if ((*it).name == name) return it;
	}
	return hydrographs.end();
}

template <typename T> T hydrograph<T>::interpolate_linear(T time)
{
	if (steps.size() == 0) return T(0);
	if (steps.size() == 1) return steps.front().second;
	if (time <= steps.front().first) return steps.front().second;
	if (time >= steps.back().first) return steps.back().second;

	for (size_t i = 0; i < steps.size() - 1; ++i) {
		auto &current = steps[i];
		auto &next = steps[i + 1];

		if (time >= current.first && time < next.first) {
			T diff = next.first - current.first;
			T left = next.first - time;
			T factor = (diff - left) / diff;
			T ret = (T(1) - factor) * current.second + factor * next.second;
			return ret;
		}
	}

	spdlog::get("console")->warn(
	    "interpolated time: " + std::to_string(time) +
	    " was greater than maximum in hydrograph:" + std::to_string(steps.back().second));
	return steps.back().second;
}
template <typename T> bool hydrograph<T>::is_valid(T max_time) const
{
	// simulation time_stop exeeds last timestep
	bool tc = static_cast<T>(steps.back().first) >= max_time;

	// timesteps must be ascending
	bool oc = true;
	for (size_t i = 0; i < steps.size() - 1; ++i) {
		auto &current = steps[i];
		auto &next = steps[i + 1];

		if (current.first >= next.first) {
			oc = false;
		}
	}
	return tc && oc;
}
}
