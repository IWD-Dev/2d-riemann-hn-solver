#include "stdafx.h"

#include "hn/state.h"
#include "hn/constants.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class state<float_t>;
template class state<double_t>;

template <typename T> std::vector<T> &state<T>::flow_dist_north() { return flow_distributions[0]; }

template <typename T> std::vector<T> &state<T>::flow_dist_east() { return flow_distributions[1]; }

template <typename T> std::vector<T> &state<T>::flow_dist_south() { return flow_distributions[2]; }

template <typename T> std::vector<T> &state<T>::flow_dist_west() { return flow_distributions[3]; }
template <typename T>
bool state<T>::write(const typename ::io::esri_header<T> &header, const config<T> &config, T time,
                     const ::util::ghost_array<T> &dem, bool dump)
{
	std::string dir_path = config.out_path;
	std::string prefix = config.result_prefix;
	std::string postfix = " - " + std::to_string(time); // just time for now

	if (dump) {
		dir_path += "dump/";
		prefix = "dmp";
	}

	std::ofstream file;

	if (config.out_depth || dump) {
		std::string path = dir_path + prefix + postfix + ".h";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s += depth(x, y) < T(DRY) ? std::to_string(header.NODATA_value) + "\t"
					                          : std::to_string(depth(x, y)) + "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write depth to path: " + path);
			return false;
		}
	}
	if (config.out_height || dump) {
		std::string path = dir_path + prefix + postfix + ".eta";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s += depth(x, y) < T(DRY) ? std::to_string(header.NODATA_value) + "\t"
					                          : std::to_string(height(x, y)) + "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write height to path: " + path);
			return false;
		}
	}
	if (config.out_x_velocity || dump) {
		std::string path = dir_path + prefix + postfix + ".u";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s += depth(x, y) < T(DRY) ? std::to_string(header.NODATA_value) + "\t"
					                          : std::to_string(x_velocity(x, y)) + "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write x-velocity to path: " + path);
			return false;
		}
	}
	if (config.out_y_velocity || dump) {
		std::string path = dir_path + prefix + postfix + ".v";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s += depth(x, y) < T(DRY) ? std::to_string(header.NODATA_value) + "\t"
					                          : std::to_string(y_velocity(x, y)) + "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write y-velocity to path: " + path);
			return false;
		}
	}
	if (config.out_vmag || dump) {
		std::string path = dir_path + prefix + postfix + ".vmag";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s += depth(x, y) < T(DRY)
					         ? std::to_string(header.NODATA_value) + "\t"
					         : std::to_string(std::sqrt(util::sqr(x_velocity(x, y)) +
					                                    util::sqr(y_velocity(x, y)))) +
					               "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write vmag to path: " + path);
			return false;
		}
	}
	if (config.out_flowd || dump) {
		std::string path = dir_path + prefix + postfix + ".fdir";
		file.open(path, std::ios::out);
		if (file.is_open()) {
			std::string s = "";
			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {
					s +=
					    depth(x, y) < T(DRY)
					        ? std::to_string(header.NODATA_value) + "\t"
					        : std::to_string(std::atan2(y_velocity(x, y), x_velocity(x, y))) + "\t";
				}
				s.pop_back(); // remove last "\t"
				s += "\n";
			}
			file << ::io::esri_header<T>::header_to_string(header);
			file << s;
			file.close();
		}
		else {
			spdlog::get("console")->warn("Could not write flowdir to path: " + path);
			return false;
		}
	}

	return true;
}
template <typename T> void state<T>::clear()
{
	height.fill(T(0));
	depth.fill(T(0));
	x_velocity.fill(T(0));
	y_velocity.fill(T(0));
	x_flow.fill(T(0));
	y_flow.fill(T(0));
}
}
