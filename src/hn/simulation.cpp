#include "stdafx.h"

#include <chrono>
#include <cmath>
#include <omp.h>

#include "hn/simulation.h"
#include "hn/constants.h"
#include "hn/reconstruction.h"
#include "hn/wetdry.h"
#include "hn/calculations.h"

#include "util/algorithm.h"
#include "util/ring_buffer.h"

namespace hn {
// explicitly define valid template types
// restrain usage and enable exporting
template class simulation<float_t>;
template class simulation<double_t>;

template <typename T> simulation<T>::simulation()
{
	// defaults dem, header
}
template <typename T> simulation<T>::~simulation() {}

template <typename T> bool simulation<T>::setup(const std::string &config_path)
{
	if (!config.load_from_json(config_path)) return false;

	std::ifstream file;
	file.open(config.in_dem, std::ios::in);
	if (file.is_open()) {
		spdlog::get("console")->info("Loading dem esri_header: " + config.in_dem);
		try {
			this->header = ::io::esri_header<T>::read_header(file);
		}
		catch (...) { // TODO: proper catch
			spdlog::get("console")->error("error reading dem esri_header: " + config.in_dem);
			return false;
		}
		file.close();
	}
	else {
		spdlog::get("console")->error("could not open dem file: " + config.in_dem);
		return false;
	}

	try {
		dem = ::util::ghost_array<T>(header.ncols, header.nrows);
		manning = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.height = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.depth = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.x_velocity = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.y_velocity = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.x_flow = ::util::ghost_array<T>(header.ncols, header.nrows);
		curr.y_flow = ::util::ghost_array<T>(header.ncols, header.nrows);

		// load elevation dem
		if (!::io::esri<T>::load(config.in_dem, dem)) return false;

		// load intial water level
		if (!::io::esri<T>::load(config.in_eta, curr.height, this->get_header())) return false;

		// load intial manning
		if (!::io::esri<T>::load(config.in_manning, manning, this->get_header())) return false;

		// calculate water height as: water level - elevation
		for (int row = 0; row < header.nrows; ++row) {
			for (int col = 0; col < header.ncols; ++col) {
				auto eta = curr.height(col, row);
				auto dm = dem(col, row);
				auto depth = eta - dm;
				if (depth <= T(DRY)) {
					depth = T{0};
					eta = T{dm};
				}
				curr.depth(col, row) = depth;
			}
		}

		// only load velocity if x and y is set, otherwise both remain 0
		if (config.in_x_velocity != "" && config.in_y_velocity != "") {
			if (!::io::esri<T>::load(config.in_x_velocity, curr.x_velocity, this->get_header()))
				return false;
			if (!::io::esri<T>::load(config.in_y_velocity, curr.y_velocity, this->get_header()))
				return false;

			// could use two seperate loops for better cache usage
			for (int row = 0; row < header.nrows; ++row) {
				for (int col = 0; col < header.ncols; ++col) {
					auto xv = curr.x_velocity(col, row);
					auto yv = curr.y_velocity(col, row);

					// comparing float with (float)int, using nearing
					if (::util::nearly_equal(xv, T(header.NODATA_value))) xv = T{0};
					if (::util::nearly_equal(yv, T(header.NODATA_value))) yv = T{0};

					curr.x_flow(col, row) = curr.depth(col, row) * xv;
					curr.y_flow(col, row) = curr.depth(col, row) * yv;
				}
			}
		}

		// setup ghost cells (x_v, y_v, x_f, y_f don't have to be if not set)
		dem.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.height.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.depth.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.x_velocity.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.y_velocity.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.x_flow.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);
		// curr.y_flow.fill_ghost_cells(::util::ghost_array_boundary_wrap::clamp);

		read = curr;
		write = curr;
		// write.clear(); // fill 0
	}
	catch (const std::exception &ex) { // TODO: proper catch
		spdlog::get("console")->error("Error in setting up initial conditions: " +
		                              std::string(ex.what()));
		return false;
	}

	if (!bc_accessor.initialize(this->get_config(), this->get_header(), curr)) return false;

	return true;
}

template <typename T> bool simulation<T>::run_sequential()
{
	// time counter variables
	size_t num_step{0};
	T time = T{0};
	long long time_console = 0ll;
	T time_out = T{0};

	// performance measurements variables
	T time_step_avg = T{0};
	auto time_step_start = std::chrono::high_resolution_clock::now();
	auto time_step_end = std::chrono::high_resolution_clock::now();
	const T time_alpha = T(0.98);

	// sim-global variables
	T dt = T(0.01);          // will be variably adjusted
	T dt_half = dt * T{0.5}; // will be variably adjusted

	const T time_stop = config.time_stop;

	const T AIMD_increase = config.time_step_AIMD_increase;
	const T AIMD_penalty = config.time_step_AIMD_penalty;
	const T AIMD_bonus = config.time_step_AIMD_bonus;
	const T dt_min = config.time_step_min;
	const T dt_max = config.time_step_max;

	const T time_console_log = config.time_sim_console_log;
	const T time_out_file = config.time_sim_out_file;

	const T courant_tolerance_low = config.courant_tolerance_low;
	const T courant_tolerance_high = config.courant_tolerance_high;

	const T dx = T(header.cellsize);
	const T dy = dx;

	T courant_max = 0;

	// variables without safe multi-thread writing
	std::map<std::string, T> hg_ipl; // interpolated hydrographs
	::util::ghost_array<T> x_courant(header.ncols, header.nrows);
	::util::ghost_array<T> y_courant(header.ncols, header.nrows);

	// Runge Kutta Coefficients
	std::array<util::ghost_array<glm::tvec3<T, glm::precision::highp>>, 4> RK_coeffs;
	for (int i = 0; i < 4; ++i)
		RK_coeffs[i] =
		    util::ghost_array<glm::tvec3<T, glm::precision::highp>>(header.ncols, header.nrows);

	// variables and arrays to hold mass balance correction for
	// individual cells for the case of h<0
	::util::ghost_array<T> height_carry(header.ncols, header.nrows);

	while (time < time_stop) {

		auto calc_time =
		    std::chrono::duration_cast<std::chrono::milliseconds>(time_step_end - time_step_start)
		        .count();
		time_step_avg =
		    time_alpha * time_step_avg + (T{1} - time_alpha) * static_cast<T>(calc_time);

		time_console += calc_time;

		// print to console
		if (util::nearly_greater_equal(static_cast<T>(time_console) / T{1000}, time_console_log)) {

			// time until simulation end = time until stop / timestep * time per step
			auto time_left = static_cast<long long>(((time_stop - time) / dt * time_step_avg) /
			                                        T{1000}); // seconds

			spdlog::get("console")->info("time: " + std::to_string(time) +
			                             " | dt: " + std::to_string(dt) +
			                             " | cmax: " + std::to_string(courant_max) +
			                             " | avg_it: " + std::to_string(calc_time) + "ms" +
			                             " | left: " + std::to_string(time_left) + "s");
			time_console = 0;
		}

		time_step_start = std::chrono::high_resolution_clock::now();

		// precalculate interpolated hydrographs
		hg_ipl.clear();
		for (auto &hg : config.hydrographs) {
			T val = hg.interpolate_linear(time);
			hg_ipl.insert(std::make_pair(hg.name, val));
			// spdlog::get("console")->info(std::to_string(val));
		}

		for (int RK = 0; RK < 4; ++RK) {

			// East
			int x = header.ncols;
			for (int y = 0; y < header.nrows; ++y) {
				const auto &bc = bc_accessor.bc_east()[y];
				auto type = bc.type;
				auto yv = read.y_velocity(x - 1, y);
				auto xv = T{};
				auto height = T{};
				switch (type) {
				case boundary::type_t::closed:
					xv = -read.x_velocity(x - 1, y);
					height = read.height(x - 1, y);
					break;
				case boundary::type_t::open:
					xv = read.x_velocity(x - 1, y);
					height = read.height(x - 1, y);
					break;
				case boundary::type_t::eta:
					xv = read.x_velocity(x - 1, y);
					height = hg_ipl[bc.hydrograph];
					break;
				case boundary::type_t::discharge:
					xv = hg_ipl[bc.hydrograph] * read.flow_dist_east()[y];
					height = read.height(x - 1, y);
					break;
				}

				read.y_velocity(x, y) = yv;
				read.x_velocity(x, y) = xv;
				read.height(x, y) = (height - dem(x, y) < T{0}) ? dem(x, y) : height;
				read.depth(x, y) = read.height(x, y) - dem(x, y);
				read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
				read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
			}

			// West
			x = -1;
			for (int y = 0; y < header.nrows; ++y) {
				const auto &bc = bc_accessor.bc_west()[y];
				auto type = bc.type;
				auto yv = read.y_velocity(x + 1, y);
				auto xv = T{};
				auto height = T{};
				switch (type) {
				case boundary::type_t::closed:
					xv = -read.x_velocity(x + 1, y);
					height = read.height(x + 1, y);
					break;
				case boundary::type_t::open:
					xv = read.x_velocity(x + 1, y);
					height = read.height(x + 1, y);
					break;
				case boundary::type_t::eta:
					xv = read.x_velocity(x + 1, y);
					height = hg_ipl[bc.hydrograph];
					break;
				case boundary::type_t::discharge:
					xv = hg_ipl[bc.hydrograph] * read.flow_dist_west()[y];
					height = read.height(x + 1, y);
					break;
				}

				read.y_velocity(x, y) = yv;
				read.x_velocity(x, y) = xv;
				read.height(x, y) = (height - dem(x, y) < T{0}) ? dem(x, y) : height;
				read.depth(x, y) = read.height(x, y) - dem(x, y);
				read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
				read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
			}

			// South
			int y = header.nrows;
			for (int x = 0; x < header.ncols; ++x) {
				const auto &bc = bc_accessor.bc_south()[x];
				auto type = bc.type;
				auto yv = T{};
				auto xv = read.x_velocity(x, y - 1);
				auto height = T{};
				switch (type) {
				case boundary::type_t::closed:
					yv = -read.y_velocity(x, y - 1);
					height = read.height(x, y - 1);
					break;
				case boundary::type_t::open:
					yv = read.y_velocity(x, y - 1);
					height = read.height(x, y - 1);
					break;
				case boundary::type_t::eta:
					yv = read.y_velocity(x, y - 1);
					height = hg_ipl[bc.hydrograph];
					break;
				case boundary::type_t::discharge:
					yv = hg_ipl[bc.hydrograph] * read.flow_dist_south()[x];
					height = read.height(x, y - 1);
					break;
				}

				read.y_velocity(x, y) = yv;
				read.x_velocity(x, y) = xv;
				read.height(x, y) = (height - dem(x, y) < T{0}) ? dem(x, y) : height;
				read.depth(x, y) = read.height(x, y) - dem(x, y);
				read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
				read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
			}

			// North
			y = -1;
			for (int x = 0; x < header.ncols; ++x) {
				const auto &bc = bc_accessor.bc_north()[x];
				auto type = bc.type;
				auto yv = T{};
				auto xv = read.y_velocity(x, y + 1);
				auto height = T{};
				switch (type) {
				case boundary::type_t::closed:
					yv = -read.y_velocity(x, y + 1);
					height = read.height(x, y + 1);
					break;
				case boundary::type_t::open:
					yv = read.y_velocity(x, y + 1);
					height = read.height(x, y + 1);
					break;
				case boundary::type_t::eta:
					yv = read.y_velocity(x, y + 1);
					height = hg_ipl[bc.hydrograph];
					break;
				case boundary::type_t::discharge:
					yv = hg_ipl[bc.hydrograph] * read.flow_dist_north()[x];
					height = read.height(x, y + 1);
					break;
				}

				read.y_velocity(x, y) = yv;
				read.x_velocity(x, y) = xv;
				read.height(x, y) = (height - dem(x, y) < T{0}) ? dem(x, y) : height;
				read.depth(x, y) = read.height(x, y) - dem(x, y);
				read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
				read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
			}

			for (int y = 0; y < header.nrows; ++y) {
				for (int x = 0; x < header.ncols; ++x) {

					// implicit friction term
					glm::tvec3<T, glm::precision::highp> U{};
					U[1] = read.x_flow(x, y);
					U[2] = read.y_flow(x, y);

					T Cf = T{0};
					T Sfx = T{0};
					T Sfy = T{0};
					T Dx = T{1};
					T Dy = T{1};
					T Fx = T{0};
					T Fy = T{0};

					if (read.depth(x, y) > T(DRY)) {
						Cf =
						    (T(G) * util::sqr(manning(x, y))) / std::pow(read.depth(x, y), T(0.33));

						auto magsqrt = std::sqrt(util::sqr(read.x_velocity(x, y)) +
						                         util::sqr(read.y_velocity(x, y)));
						Sfx = Cf * read.x_velocity(x, y) * magsqrt;
						Sfy = Cf * read.y_velocity(x, y) * magsqrt;

						auto dpsq = util::sqr(read.depth(x, y));
						Dx = T{1} + T{2} * dt * Cf * std::fabs(U[1]) / dpsq;
						Dy = T{1} + T{2} * dt * Cf * std::fabs(U[2]) / dpsq;

						Fx = Sfx / Dx;
						Fy = Sfy / Dy;
					}

					// update fluxes
					glm::tvec3<T, glm::precision::highp> Up{}; // why not use tec2 here?
					T Sfxp{}, Sfyp{};

					switch (RK) { // what to do in RK 1 - 4
					case 0:
					// Fallthrough
					case 1:
						Up[1] = U[1] + dt_half * Fx;
						Up[2] = U[2] + dt_half * Fy;
						// limit friction force
						if (Up[1] * U[1] < T{0}) Up[1] = U[1];
						if (Up[2] * U[2] < T{0}) Up[2] = U[2];
						// compute Sf(t+1)
						Sfxp = (Up[1] - U[1]) / dt_half;
						Sfyp = (Up[2] - U[2]) / dt_half;
						break;
					case 2:
					// Fallthrough
					case 3:
						Up[1] = U[1] + dt * Fx;
						Up[2] = U[2] + dt * Fy;
						// limit friction force
						if (Up[1] * U[1] < T{0}) Up[1] = U[1];
						if (Up[2] * U[2] < T{0}) Up[2] = U[2];
						// compute Sf(t+1)
						Sfxp = (Up[1] - U[1]) / dt;
						Sfyp = (Up[2] - U[2]) / dt;
						break;
					}

					// reconstruct face values
					// -----------------------------------------------------------------------
					// temporary variables for face values;
					hn::reconstruction<T> recon{};

					// dry cell? -> 1st order reconstruction, zero inner velocities and
					// fluxes
					if (read.depth(x, y) <= T(DRY)) {

						// TODO: this seems right for now, save a lot of calc time on dry area
						T max_neighbor_depth =
						    std::max({read.depth(x + 1, y), read.depth(x - 1, y),
						              read.depth(x, y - 1), read.depth(x, y + 1)});
						if (max_neighbor_depth < T(DRY)) continue;

						recon.fill_1dry(read.height, read.depth, read.x_velocity, read.y_velocity,
						                read.x_flow, read.y_flow, dem, x, y);
					}
					else { // wet cell
						T min_neighbor_depth =
						    std::min({read.depth(x + 1, y), read.depth(x - 1, y),
						              read.depth(x, y - 1), read.depth(x, y + 1)});

						// wet cell with at least one dry neighbor? -> 1st order reconst.
						if (min_neighbor_depth <= T(DRY)) {
							recon.fill_1drywet(read.height, read.depth, read.x_velocity,
							                   read.y_velocity, read.x_flow, read.y_flow, dem, x,
							                   y);
						}
						else { // wet cell with wet neighbors -> 2nd order
							recon.fill_2inn(reconstruction<T>::dtype::eta, read.height, dx, dy, x,
							                y);
							recon.fill_2inn(reconstruction<T>::dtype::h, read.depth, dx, dy, x, y);
							recon.fill_2inn(reconstruction<T>::dtype::qx, read.x_flow, dx, dy, x,
							                y);
							recon.fill_2inn(reconstruction<T>::dtype::qy, read.y_flow, dx, dy, x,
							                y);

							recon.fill_2out(reconstruction<T>::dtype::eta, read.height, dx, dy, x,
							                y, header.ncols, header.nrows);
							recon.fill_2out(reconstruction<T>::dtype::h, read.depth, dx, dy, x, y,
							                header.ncols, header.nrows);
							recon.fill_2out(reconstruction<T>::dtype::qx, read.x_flow, dx, dy, x, y,
							                header.ncols, header.nrows);
							recon.fill_2out(reconstruction<T>::dtype::qy, read.y_flow, dx, dy, x, y,
							                header.ncols, header.nrows);

							recon.recalculate(); // calc zb, u, v
						}
					}

					// wetting-drying (Liang 2010)
					// -----------------------------------------------------------------------
					wetdry<T> wd{};
					wd.wet_dry(recon);

					// HLLC Riemann fluxes (Toro 2001)
					// -----------------------------------------------------------------------
					// thus far the most expensive calculation
					auto fluxes = calculations<T>::flux(recon, wd);

					// Update cell
					// -----------------------------------------------------------------------

					glm::tvec3<T, glm::precision::highp> S{};

					S[0] = T{0};
					S[1] = ((-T(G_2) * (wd.etaRwWD + wd.etaLeWD) * (wd.zbe - wd.zbw)) / dx) - Sfxp;
					S[2] = ((-T(G_2) * (wd.etaDnWD + wd.etaUsWD) * (wd.zbn - wd.zbs)) / dy) - Sfyp;

					glm::tvec3<T, glm::precision::highp> rk_val{};
					// calculate RK coefficients and store them
					for (int i = 0; i < 3; ++i) {
						// coefficients at RK step, at pos xy,  from 0 to 2
						rk_val[i] = -(fluxes[0][i] - fluxes[1][i]) / dx -
						            (fluxes[2][i] - fluxes[3][i]) / dy + S[i];
						RK_coeffs[RK](x, y)[i] = rk_val[i];
					}

					switch (RK) { // what to do in RK 1 - 4
					case 0:
					// Fallthrough because RK1 == RK2 calc
					case 1:
						write.height(x, y) = curr.height(x, y) + T(0.5) * dt * rk_val[0];
						write.x_flow(x, y) = curr.x_flow(x, y) + T(0.5) * dt * rk_val[1];
						write.y_flow(x, y) = curr.y_flow(x, y) + T(0.5) * dt * rk_val[2];
						write.depth(x, y) = write.height(x, y) - dem(x, y);
						if (write.depth(x, y) <= T(DRY)) {
							write.x_velocity(x, y) = T{0};
							write.y_velocity(x, y) = T{0};
							write.x_flow(x, y) = T{0};
							write.y_flow(x, y) = T{0};
							write.depth(x, y) = T{0};
							write.height(x, y) = dem(x, y);
						}
						else {
							write.x_velocity(x, y) = write.x_flow(x, y) / write.depth(x, y);
							write.y_velocity(x, y) = write.y_flow(x, y) / write.depth(x, y);
						}
						break;
					case 2:
						write.height(x, y) = curr.height(x, y) + dt * rk_val[0];
						write.x_flow(x, y) = curr.x_flow(x, y) + dt * rk_val[1];
						write.y_flow(x, y) = curr.y_flow(x, y) + dt * rk_val[2];
						write.depth(x, y) = write.height(x, y) - dem(x, y);
						if (write.depth(x, y) <= T(DRY)) {
							write.x_velocity(x, y) = T{0};
							write.y_velocity(x, y) = T{0};
							write.x_flow(x, y) = T{0};
							write.y_flow(x, y) = T{0};
							write.depth(x, y) = T{0};
							write.height(x, y) = dem(x, y);
						}
						else {
							write.x_velocity(x, y) = write.x_flow(x, y) / write.depth(x, y);
							write.y_velocity(x, y) = write.y_flow(x, y) / write.depth(x, y);
						}
						break;
					case 3:
						glm::tvec3<T, glm::precision::highp> RK_Up{};
						RK_Up[0] = curr.height(x, y) +
						           T(1.0 / 6.0) * dt *
						               (RK_coeffs[0](x, y)[0] + T{2} * RK_coeffs[1](x, y)[0] +
						                T{2} * RK_coeffs[2](x, y)[0] + RK_coeffs[3](x, y)[0]);
						RK_Up[1] = curr.x_flow(x, y) +
						           T(1.0 / 6.0) * dt *
						               (RK_coeffs[0](x, y)[1] + T{2} * RK_coeffs[1](x, y)[1] +
						                T{2} * RK_coeffs[2](x, y)[1] + RK_coeffs[3](x, y)[1]);
						RK_Up[2] = curr.y_flow(x, y) +
						           T(1.0 / 6.0) * dt *
						               (RK_coeffs[0](x, y)[2] + T{2} * RK_coeffs[1](x, y)[2] +
						                T{2} * RK_coeffs[2](x, y)[2] + RK_coeffs[3](x, y)[2]);

						// update Primitives, write final state for this iteration
						calculations<T>::updatePrimitives(write, dem, height_carry, RK_Up, x, y);

						// compute Courant number and update it if new cell > previous cell
						auto cx = std::fabs(write.x_velocity(x, y)) +
						          std::sqrt(T(G) * std::fabs(write.depth(x, y)));

						if (cx != T{0})
							x_courant(x, y) = dt / (dx / cx);
						else
							x_courant(x, y) = T{0};

						auto cy = std::fabs(write.y_velocity(x, y)) +
						          std::sqrt(T(G) * std::fabs(write.depth(x, y)));
						if (cy != T{0})
							y_courant(x, y) = dt / (dy / cy);
						else
							y_courant(x, y) = T{0};

						if (cx != cx || cy > cy) { // numerical instability
							spdlog::get("console")->error(
							    "Simulation became unstable at x: " + std::to_string(x) +
							    " y: " + std::to_string(x) + "!");
							return false;
						}

						break;
					} // end RK switch

				} // for x

			} // for y

			std::swap(read, write);

		} // END RK ITERATION

		// safety clear
		// for (int i = 0; i < 4; ++i)
		//	RK_coeffs[i].fill(glm::tvec3<T, glm::precision::highp>(T(0)));

		// calculate courant number
		auto cx_max_it = std::max_element(x_courant.begin(), x_courant.end());
		auto cy_max_it = std::max_element(y_courant.begin(), y_courant.end());
		courant_max = std::max(*(cx_max_it), *(cy_max_it));

		// if (courant_max > courant_tolerance_high) {
		//	spdlog::get("console")->warn("Courant Max higher than 1.0!");
		//}

		// TODO: remove comment to activate
		dt = courant_max >= courant_tolerance_high
		         ? dt / (courant_max + (courant_max * AIMD_penalty))
		         : courant_max < courant_tolerance_low ? dt * AIMD_bonus : dt + AIMD_increase;

		dt = ::util::clamp(dt, dt_min, dt_max);
		dt_half = dt / T{2};

		curr = read;

		// write to disk
		if (time_out >= time_out_file) {
			curr.write(header, config, std::round(time), dem);
			time_out = 0;
		}

		// advance time
		num_step++;
		time += dt;
		time_out += dt;

		time_step_end = std::chrono::high_resolution_clock::now();

	} // end while loop

	return true;
}

template <typename T> bool simulation<T>::run_parallel()
{
	// time counter variables
	size_t num_step{0};
	T time = T{0};
	T time_out = T{0};
	T time_state_buffer = T{0};
	long long time_console = 0ll;

	// performance measurements variables
	T time_step_avg = T{0};
	auto time_step_start = std::chrono::high_resolution_clock::now();
	auto time_step_end = std::chrono::high_resolution_clock::now();
	const T time_alpha = T(0.98);

	// sim-global variables

	T dt = T(0.01);          // will be variably adjusted
	T dt_half = dt * T{0.5}; // will be variably adjusted

	// const copy from config for better access times
	const T time_stop = config.time_stop;

	const T AIMD_increase = config.time_step_AIMD_increase;
	const T AIMD_penalty = config.time_step_AIMD_penalty;
	const T AIMD_bonus = config.time_step_AIMD_bonus;
	const T dt_min = config.time_step_min;
	const T dt_max = config.time_step_max;

	const T time_console_log = config.time_sim_console_log;
	const T time_out_file = config.time_sim_out_file;

	const T courant_tolerance_low = config.courant_tolerance_low;
	const T courant_tolerance_high = config.courant_tolerance_high;

	const T time_rollback = config.time_rollback;
	const size_t rollback_strikes = config.rollback_strikes;
	const size_t rollback_count = config.rollback_count;

	const T dx = T(header.cellsize);
	const T dy = dx;

	T courant_max = T{0};
	size_t rollback_strike = 0ull;
	bool stable = true; // flag for all threads if simulation is still stable
	bool stable_skip = false;

	// variables without safe multi-thread writing
	// use omp single for now (might change)
	std::map<std::string, T> hg_ipl; // interpolated hydrographs
	::util::ghost_array<T> x_courant(header.ncols, header.nrows);
	::util::ghost_array<T> y_courant(header.ncols, header.nrows);

	struct state_buffer_struct {
		T time{};
		T courant{};
		state<T> state;
	};
	struct {
		bool operator()(const state_buffer_struct &lhs, const state_buffer_struct &rhs) const
		{
			return lhs.courant < rhs.courant;
		}
	} state_buffer_less_sort;
	::util::ring_buffer<state_buffer_struct> state_buffer(rollback_count);

	// Runge Kutta Coefficients
	std::array<util::ghost_array<glm::tvec3<T, glm::precision::highp>>, 4> RK_coeffs;
	for (int i = 0; i < 4; ++i)
		RK_coeffs[i] =
		    util::ghost_array<glm::tvec3<T, glm::precision::highp>>(header.ncols, header.nrows);

	// variables and arrays to hold mass balance correction for
	// individual cells for the case of h<0
	::util::ghost_array<T> height_carry(header.ncols, header.nrows);

	// parallely written temporary variables
	::util::ghost_array<unsigned char> dry_skip(header.ncols, header.nrows);
	::util::ghost_array<T> Sfxp(header.ncols, header.nrows);
	::util::ghost_array<T> Sfyp(header.ncols, header.nrows);
	reconstruction_parallel<T> recon(header.ncols, header.nrows);
	wetdry_parallel<T> wd(header.ncols, header.nrows);
	::util::ghost_array<std::array<glm::tvec3<T, glm::precision::highp>, 4>> fluxes(header.ncols,
	                                                                                header.nrows);

#pragma omp parallel
	{

		while (time < time_stop && stable) {

#pragma omp single
			{
				auto calc_time = std::chrono::duration_cast<std::chrono::milliseconds>(
				                     time_step_end - time_step_start)
				                     .count();
				time_step_avg =
				    time_alpha * time_step_avg + (T{1} - time_alpha) * static_cast<T>(calc_time);

				time_console += calc_time;

				// print to console
				if (util::nearly_greater_equal(static_cast<T>(time_console) / T{1000},
				                               time_console_log)) {

					// time until simulation end = time until stop / timestep * time per step
					auto time_left = static_cast<long long>(
					    ((time_stop - time) / dt * time_step_avg) / T{1000}); // seconds

					spdlog::get("console")->info("time: " + std::to_string(time) +
					                             " | dt: " + std::to_string(dt) +
					                             " | cfl: " + std::to_string(courant_max) +
					                             " | iter: " + std::to_string(calc_time) + "ms" +
					                             " | left: " + std::to_string(time_left) + "s");
					time_console = 0;
				}

				time_step_start = std::chrono::high_resolution_clock::now();

				// precalculate interpolated hydrographs
				hg_ipl.clear();
				for (auto &hg : config.hydrographs) {
					T val = hg.interpolate_linear(time);
					hg_ipl.insert(std::make_pair(hg.name, val));
				}

			} // end single

#pragma omp barrier

			for (int RK = 0; RK < 4; ++RK) {

				// Prepare ghost cells

				// East
				int x = header.ncols;
#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					const auto &bc = bc_accessor.bc_east()[y];
					auto type = bc.type;
					auto yv = read.y_velocity(x - 1, y);
					auto xv = T{};
					auto height = T{};
					switch (type) {
					case boundary::type_t::closed:
						xv = -read.x_velocity(x - 1, y);
						height = read.height(x - 1, y);
						break;
					case boundary::type_t::open:
						xv = read.x_velocity(x - 1, y);
						height = read.height(x - 1, y);
						break;
					case boundary::type_t::eta:
						xv = read.x_velocity(x - 1, y);
						height = hg_ipl[bc.hydrograph];
						break;
					case boundary::type_t::discharge:
						xv = hg_ipl[bc.hydrograph] * read.flow_dist_east()[y];
						height = read.height(x - 1, y);
						break;
					}

					read.y_velocity(x, y) = yv;
					read.x_velocity(x, y) = xv;
					read.height(x, y) = height;
					read.depth(x, y) = read.height(x, y) - dem(x, y);
					read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
					read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
				}

				// West
				x = -1;
#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					const auto &bc = bc_accessor.bc_west()[y];
					auto type = bc.type;
					auto yv = read.y_velocity(x + 1, y);
					auto xv = T{};
					auto height = T{};
					switch (type) {
					case boundary::type_t::closed:
						xv = -read.x_velocity(x + 1, y);
						height = read.height(x + 1, y);
						break;
					case boundary::type_t::open:
						xv = read.x_velocity(x + 1, y);
						height = read.height(x + 1, y);
						break;
					case boundary::type_t::eta:
						xv = read.x_velocity(x + 1, y);
						height = hg_ipl[bc.hydrograph];
						break;
					case boundary::type_t::discharge:
						xv = hg_ipl[bc.hydrograph] * read.flow_dist_west()[y];
						height = read.height(x + 1, y);
						break;
					}

					read.y_velocity(x, y) = yv;
					read.x_velocity(x, y) = xv;
					read.height(x, y) = height;
					read.depth(x, y) = read.height(x, y) - dem(x, y);
					read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
					read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
				}

				// South
				int y = header.nrows;
#pragma omp for schedule(static, 4)
				for (int x = 0; x < header.ncols; ++x) {
					const auto &bc = bc_accessor.bc_south()[x];
					auto type = bc.type;
					auto yv = T{};
					auto xv = read.x_velocity(x, y - 1);
					auto height = T{};
					switch (type) {
					case boundary::type_t::closed:
						yv = -read.y_velocity(x, y - 1);
						height = read.height(x, y - 1);
						break;
					case boundary::type_t::open:
						yv = read.y_velocity(x, y - 1);
						height = read.height(x, y - 1);
						break;
					case boundary::type_t::eta:
						yv = read.y_velocity(x, y - 1);
						height = hg_ipl[bc.hydrograph];
						break;
					case boundary::type_t::discharge:
						yv = hg_ipl[bc.hydrograph] * read.flow_dist_south()[x];
						height = read.height(x, y - 1);
						break;
					}

					read.y_velocity(x, y) = yv;
					read.x_velocity(x, y) = xv;
					read.height(x, y) = height;
					read.depth(x, y) = read.height(x, y) - dem(x, y);
					read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
					read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
				}

				// North
				y = -1;
#pragma omp for schedule(static, 4)
				for (int x = 0; x < header.ncols; ++x) {
					const auto &bc = bc_accessor.bc_north()[x];
					auto type = bc.type;
					auto yv = T{};
					auto xv = read.y_velocity(x, y + 1);
					auto height = T{};
					switch (type) {
					case boundary::type_t::closed:
						yv = -read.y_velocity(x, y + 1);
						height = read.height(x, y + 1);
						break;
					case boundary::type_t::open:
						yv = read.y_velocity(x, y + 1);
						height = read.height(x, y + 1);
						break;
					case boundary::type_t::eta:
						yv = read.y_velocity(x, y + 1);
						height = hg_ipl[bc.hydrograph];
						break;
					case boundary::type_t::discharge:
						yv = hg_ipl[bc.hydrograph] * read.flow_dist_north()[x];
						height = read.height(x, y + 1);
						break;
					}

					read.y_velocity(x, y) = yv;
					read.x_velocity(x, y) = xv;
					read.height(x, y) = height;
					read.depth(x, y) = read.height(x, y) - dem(x, y);
					read.x_flow(x, y) = read.x_velocity(x, y) * read.depth(x, y);
					read.y_flow(x, y) = read.y_velocity(x, y) * read.depth(x, y);
				}

// calculate skippable cells
#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (read.depth(x, y) <= T(DRY)) {
							T max_neighbor_depth =
							    std::max({read.depth(x + 1, y), read.depth(x - 1, y),
							              read.depth(x, y - 1), read.depth(x, y + 1)});

							dry_skip(x, y) = max_neighbor_depth <= T(DRY);
						}
					}
				}

// calculate friction terms
// -----------------------------------------------------------------------

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;

						glm::tvec3<T, glm::precision::highp> U{};
						U[1] = read.x_flow(x, y);
						U[2] = read.y_flow(x, y);

						T Cf = T{0};
						T Sfx = T{0};
						T Sfy = T{0};
						T Dx = T{1};
						T Dy = T{1};
						T Fx = T{0};
						T Fy = T{0};

						if (read.depth(x, y) > T(DRY)) {
							Cf = (T(G) * util::sqr(manning(x, y))) /
							     std::pow(read.depth(x, y), T(0.33));

							auto magsqrt = std::sqrt(util::sqr(read.x_velocity(x, y)) +
							                         util::sqr(read.y_velocity(x, y)));
							Sfx = Cf * read.x_velocity(x, y) * magsqrt;
							Sfy = Cf * read.y_velocity(x, y) * magsqrt;

							auto dpsq = util::sqr(read.depth(x, y));
							Dx = T{1} + T{2} * dt * Cf * std::fabs(U[1]) / dpsq;
							Dy = T{1} + T{2} * dt * Cf * std::fabs(U[2]) / dpsq;

							Fx = Sfx / Dx;
							Fy = Sfy / Dy;
						}

						// update fluxes
						glm::tvec3<T, glm::precision::highp> Up{}; // why not use tec2 here?

						switch (RK) { // what to do in RK 1 - 4
						case 0:
						// Fallthrough
						case 1:
							Up[1] = U[1] + dt_half * Fx;
							Up[2] = U[2] + dt_half * Fy;
							// limit friction force
							if (Up[1] * U[1] < T{0}) Up[1] = U[1];
							if (Up[2] * U[2] < T{0}) Up[2] = U[2];
							// compute Sf(t+1)
							Sfxp(x, y) = (Up[1] - U[1]) / dt_half;
							Sfyp(x, y) = (Up[2] - U[2]) / dt_half;
							break;
						case 2:
						// Fallthrough
						case 3:
							Up[1] = U[1] + dt * Fx;
							Up[2] = U[2] + dt * Fy;
							// limit friction force
							if (Up[1] * U[1] < T{0}) Up[1] = U[1];
							if (Up[2] * U[2] < T{0}) Up[2] = U[2];
							// compute Sf(t+1)
							Sfxp(x, y) = (Up[1] - U[1]) / dt;
							Sfyp(x, y) = (Up[2] - U[2]) / dt;
							break;
						}
					}
				}

// reconstruct face values
// -----------------------------------------------------------------------

// TODO: split into seperate loops
// 1st pass: combine dry and drywet
// 2nd pass: wet
// 3rd pass: recalc u v zb

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;

						// dry cell? -> 1st order reconstruction, zero inner velocities and
						// fluxes
						if (read.depth(x, y) <= T(DRY)) {
							recon.fill_1dry_eta(read.height, x, y);
							recon.fill_1dry_h(read.depth, x, y);
							recon.fill_1dry_u(read.x_velocity, x, y);
							recon.fill_1dry_v(read.y_velocity, x, y);
							recon.fill_1dry_qx(read.x_flow, x, y);
							recon.fill_1dry_qy(read.y_flow, x, y);
							recon.fill_1dry_zb(dem, x, y);
						}
						else { // wet cell
							T min_neighbor_depth =
							    std::min({read.depth(x + 1, y), read.depth(x - 1, y),
							              read.depth(x, y - 1), read.depth(x, y + 1)});

							// wet cell with at least one dry neighbor? -> 1st order reconst.
							if (min_neighbor_depth <= T(DRY)) {
								recon.fill_1drywet_eta(read.height, x, y);
								recon.fill_1drywet_h(read.depth, x, y);
								recon.fill_1drywet_u(read.x_velocity, x, y);
								recon.fill_1drywet_v(read.y_velocity, x, y);
								recon.fill_1drywet_qx(read.x_flow, x, y);
								recon.fill_1drywet_qy(read.y_flow, x, y);
								recon.fill_1drywet_zb(dem, x, y);
							}
							else { // wet cell with wet neighbors -> 2nd order
								recon.fill_2inn_eta(read.height, dx, dy, x, y);
								recon.fill_2out_eta(read.height, dx, dy, x, y, header.ncols,
								                    header.nrows);

								recon.fill_2inn_h(read.depth, dx, dy, x, y);
								recon.fill_2out_h(read.depth, dx, dy, x, y, header.ncols,
								                  header.nrows);

								recon.fill_2inn_qx(read.x_flow, dx, dy, x, y);
								recon.fill_2out_qx(read.x_flow, dx, dy, x, y, header.ncols,
								                   header.nrows);

								recon.fill_2inn_qy(read.y_flow, dx, dy, x, y);
								recon.fill_2out_qy(read.y_flow, dx, dy, x, y, header.ncols,
								                   header.nrows);

								recon.recalculate_u(x, y);
								recon.recalculate_v(x, y);
								recon.recalculate_zb(x, y);
							}
						}
					}
				}

// wetting-drying (Liang 2010)
// -----------------------------------------------------------------------

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						wd.wet_dry_east(recon, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						wd.wet_dry_west(recon, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						wd.wet_dry_north(recon, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						wd.wet_dry_south(recon, x, y);
					}
				}

// HLLC Riemann fluxes (Toro 2001)
// -----------------------------------------------------------------------

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						fluxes(x, y)[0] = calculations_parallel<T>::flux_east(recon, wd, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						fluxes(x, y)[1] = calculations_parallel<T>::flux_west(recon, wd, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						fluxes(x, y)[2] = calculations_parallel<T>::flux_north(recon, wd, x, y);
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;
						fluxes(x, y)[3] = calculations_parallel<T>::flux_south(recon, wd, x, y);
					}
				}

// Update cell
// -----------------------------------------------------------------------

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;

						T S{0};
						T rk_val = -(fluxes(x, y)[0][0] - fluxes(x, y)[1][0]) / dx -
						           (fluxes(x, y)[2][0] - fluxes(x, y)[3][0]) / dy + S;
						RK_coeffs[RK](x, y)[0] = rk_val;

						switch (RK) { // what to do in RK 1 - 4
						case 0:
						// Fallthrough because RK1 == RK2 calc
						case 1:
							write.height(x, y) = curr.height(x, y) + T(0.5) * dt * rk_val;
							break;
						case 2:
							write.height(x, y) = curr.height(x, y) + dt * rk_val;
							break;
						case 3:
							// calculating RK_coeffs is enough
							break;
						}
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;

						T S = ((-T(G_2) * (wd.etaRwWD(x, y) + wd.etaLeWD(x, y)) *
						        (wd.zbe(x, y) - wd.zbw(x, y))) /
						       dx) -
						      Sfxp(x, y);
						T rk_val = -(fluxes(x, y)[0][1] - fluxes(x, y)[1][1]) / dx -
						           (fluxes(x, y)[2][1] - fluxes(x, y)[3][1]) / dy + S;
						RK_coeffs[RK](x, y)[1] = rk_val;

						switch (RK) { // what to do in RK 1 - 4
						case 0:
						// Fallthrough because RK1 == RK2 calc
						case 1:
							write.x_flow(x, y) = curr.x_flow(x, y) + T(0.5) * dt * rk_val;
							break;
						case 2:
							write.x_flow(x, y) = curr.x_flow(x, y) + dt * rk_val;
							break;
						case 3:
							// calculating RK_coeffs is enough
							break;
						}
					}
				}

#pragma omp for schedule(static, 4)
				for (int y = 0; y < header.nrows; ++y) {
					for (int x = 0; x < header.ncols; ++x) {
						if (dry_skip(x, y)) continue;

						T S = ((-T(G_2) * (wd.etaDnWD(x, y) + wd.etaUsWD(x, y)) *
						        (wd.zbn(x, y) - wd.zbs(x, y))) /
						       dy) -
						      Sfyp(x, y);
						T rk_val = -(fluxes(x, y)[0][2] - fluxes(x, y)[1][2]) / dx -
						           (fluxes(x, y)[2][2] - fluxes(x, y)[3][2]) / dy + S;
						RK_coeffs[RK](x, y)[2] = rk_val;

						switch (RK) { // what to do in RK 1 - 4
						case 0:
						// Fallthrough because RK1 == RK2 calc
						case 1:
							write.y_flow(x, y) = curr.y_flow(x, y) + T(0.5) * dt * rk_val;
							break;
						case 2:
							write.y_flow(x, y) = curr.y_flow(x, y) + dt * rk_val;
							break;
						case 3:
							// calculating RK_coeffs is enough
							break;
						}
					}
				}

				if (RK < 3) {

#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							if (dry_skip(x, y)) continue;

							write.depth(x, y) = write.height(x, y) - dem(x, y);
						}
					}

#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							if (dry_skip(x, y)) continue;

							if (write.depth(x, y) <= T(DRY)) {
								write.x_velocity(x, y) = T{0};
								write.y_velocity(x, y) = T{0};
								write.x_flow(x, y) = T{0};
								write.y_flow(x, y) = T{0};
								write.depth(x, y) = T{0};
								write.height(x, y) = dem(x, y);
							}
							else {
								write.x_velocity(x, y) = write.x_flow(x, y) / write.depth(x, y);
								write.y_velocity(x, y) = write.y_flow(x, y) / write.depth(x, y);
							}
						}
					}
				}
				else { // RK == 3, update Primitives

#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							if (dry_skip(x, y)) continue;

							write.height(x, y) =
							    curr.height(x, y) +
							    T(1.0 / 6.0) * dt *
							        (RK_coeffs[0](x, y)[0] + T{2} * RK_coeffs[1](x, y)[0] +
							         T{2} * RK_coeffs[2](x, y)[0] + RK_coeffs[3](x, y)[0]);
						}
					}
#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							if (dry_skip(x, y)) continue;

							write.x_flow(x, y) =
							    curr.x_flow(x, y) +
							    T(1.0 / 6.0) * dt *
							        (RK_coeffs[0](x, y)[1] + T{2} * RK_coeffs[1](x, y)[1] +
							         T{2} * RK_coeffs[2](x, y)[1] + RK_coeffs[3](x, y)[1]);
						}
					}
#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							if (dry_skip(x, y)) continue;

							write.y_flow(x, y) =
							    curr.y_flow(x, y) +
							    T(1.0 / 6.0) * dt *
							        (RK_coeffs[0](x, y)[2] + T{2} * RK_coeffs[1](x, y)[2] +
							         T{2} * RK_coeffs[2](x, y)[2] + RK_coeffs[3](x, y)[2]);
						}
					}
#pragma omp for schedule(static, 4)
					for (int y = 0; y < header.nrows; ++y) {
						for (int x = 0; x < header.ncols; ++x) {
							write.depth(x, y) = write.height(x, y) - dem(x, y);
						}
					}

#pragma omp single
					{

						for (int y = 0; y < header.nrows; ++y) {
							for (int x = 0; x < header.ncols; ++x) {
								if (stable_skip || dry_skip(x, y)) continue;

								// update Primitives, write final state for this iteration
								calculations_parallel<T>::updatePrimitives(write, dem, height_carry,
								                                           x, y);

								auto c = (std::fabs(write.x_velocity(x, y)) * dt / dx) +
								         (std::fabs(write.y_velocity(x, y)) * dt / dy);

								// compute Courant number and update it if new cell > previous cell
								// auto cx = std::fabs(write.x_velocity(x, y)) +
								//          std::sqrt(T(G) * std::fabs(write.depth(x, y)));
								x_courant(x, y) = c;

								// auto cy = std::fabs(write.y_velocity(x, y)) +
								//          std::sqrt(T(G) * std::fabs(write.depth(x, y)));
								y_courant(x, y) = c;

								if (stable && (c != c || c > c)) { // numerical instability
									spdlog::get("console")->warn(
									    "Simulation became unstable at x: " + std::to_string(x) +
									    " y: " + std::to_string(y) + "! Dumping current state!");

									if (rollback_count != 0) {
										if (rollback_strike < rollback_strikes) {
											auto sb = state_buffer.get();
											if (!sb.empty()) {
												std::sort(sb.begin(), sb.end(),
												          state_buffer_less_sort);

												write = sb[0].state; // rollback to most stable
												                     // saved state
												time = sb[0].time;

												spdlog::get("console")->warn(
												    "Rollback to time: " +
												    std::to_string(sb[0].time) + ". Strike " +
												    std::to_string(rollback_strike + 1ull) + "/" +
												    std::to_string(rollback_strikes) + "!");

												dt = T(0.0001);
												dt_half = dt / T{2};
												stable_skip = true;
											}
											else {
												spdlog::get("console")->error(
												    "Simulation became unstable "
												    "before a valid save state "
												    "could be set!");
												stable = false;
											}
											++rollback_strike;
										}
										else {
											spdlog::get("console")->error(
											    "Maximum rollback strikes exceeded!");
											stable = false;
										}
									}
									else { // no rollback
										stable = false;
										stable_skip = true;
									}

									// dump error state
									write.write(header, config, std::floor(time), dem, true);
								}
							}
						}
					}
				}

#pragma omp single
				{
					// swap is way slower than a simple assign
					// std::swap(read, write);
					read = write;
				}

#pragma omp barrier
			} // END RK ITERATION

#pragma omp single
			{

				// calculate courant number
				auto cx_max_it = std::max_element(x_courant.begin(), x_courant.end());
				auto cy_max_it = std::max_element(y_courant.begin(), y_courant.end());
				courant_max = std::max(*(cx_max_it), *(cy_max_it));

				dt = courant_max >= courant_tolerance_high
				         ? dt * (std::max(T(0.01), T{1} - AIMD_penalty))
				         : courant_max < courant_tolerance_low ? dt * AIMD_bonus
				                                               : dt + AIMD_increase;

				// this seems to provoke instabilites
				// if (courant_max >= T(1.0)) dt /= T{10};

				dt = ::util::clamp(dt, dt_min, dt_max);
				dt_half = dt / T{2};

				curr = read;

				if (rollback_count != 0 && time_state_buffer >= time_rollback) {
					state_buffer_struct sb{time, courant_max, curr};
					state_buffer.push(sb);

					T intp;
					time_state_buffer = std::modf(time_state_buffer, &intp);
				}

				// write to disk
				if (time_out >= time_out_file) {
					curr.write(header, config, std::floor(time), dem);

					T intp;
					time_out = std::modf(time_out, &intp);
				}

				// advance time
				num_step++;
				time += dt;
				time_state_buffer += dt;
				time_out += dt;

				time_step_end = std::chrono::high_resolution_clock::now();
			}

#pragma omp barrier

		} // end while loop
	}     // end parallel section

	// write end state
	curr.write(header, config, std::floor(time), dem);

	return stable;
}
} // namespace hn
