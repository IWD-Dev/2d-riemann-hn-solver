#include "stdafx.h"

#include "hn/boundary.h"
#include "hn/config.h"
#include "hn/constants.h"
#include "util/algorithm.h"

namespace hn {

// explicitly define valid template types
// restrain usage and enable exporting
template class boundary::boundary_condition_accessor<float_t>;
template class boundary::boundary_condition_accessor<double_t>;

boundary::edge_t boundary::str_to_edge(const std::string &str)
{
	if (str == "N") return hn::boundary::edge_t::N;
	if (str == "E") return hn::boundary::edge_t::E;
	if (str == "S") return hn::boundary::edge_t::S;
	if (str == "W") return hn::boundary::edge_t::W;
	return hn::boundary::edge_t::unassigned;
}

std::string boundary::edge_to_str(const edge_t &edge)
{
	if (edge == hn::boundary::edge_t::N) return "N";
	if (edge == hn::boundary::edge_t::E) return "E";
	if (edge == hn::boundary::edge_t::S) return "S";
	if (edge == hn::boundary::edge_t::W) return "W";
	return "unassigned";
}

boundary::type_t boundary::str_to_type(const std::string &str)
{
	if (str == "open") return hn::boundary::type_t::open;
	if (str == "closed") return hn::boundary::type_t::closed;
	if (str == "discharge") return hn::boundary::type_t::discharge;
	if (str == "eta") return hn::boundary::type_t::eta;
	return hn::boundary::type_t::unassigned;
}

std::string boundary::type_to_str(const type_t &type)
{
	if (type == hn::boundary::type_t::closed) return "closed";
	if (type == hn::boundary::type_t::open) return "open";
	if (type == hn::boundary::type_t::discharge) return "discharge";
	if (type == hn::boundary::type_t::eta) return "eta";
	return "unassigned";
}

template <typename T>
bool boundary::boundary_condition_accessor<T>::initialize(const config<T> &config,
                                                          const typename io::esri_header<T> header,
                                                          state<T> &state)
{
	boundary_conditions[0].resize(header.ncols); // N
	boundary_conditions[1].resize(header.nrows); // E
	boundary_conditions[2].resize(header.ncols); // S
	boundary_conditions[3].resize(header.nrows); // W

	// validate hydrographs
	for (auto &hg : config.hydrographs) {
		if (!hg.is_valid(config.time_stop)) {
			spdlog::get("console")->error("Hydrograph: " + hg.name +
			                              " is invalid! Last step ends before simulation "
			                              "stops or time is not ascending.");
			return false;
		}
	}

	// validate bc start < end
	// extra loop for clearer code (otherwise could be in loop below)
	int bc_counter = 0;
	for (auto &bc : config.boundary_conditions) {
		if (bc.start >= bc.end) {
			spdlog::get("console")->error("Error at Boundary condition #" +
			                              std::to_string(bc_counter) +
			                              ". Start is bigger than End!");
			return false;
		}
		++bc_counter;
	}

	// for every N E S W
	// create boundary_conditions in matching cells
	// fill boundary_condition_accessor internal boundary_conditions
	bc_counter = 0;
	for (auto &bc : config.boundary_conditions) {

		// pointer is guaranteed to be not nullptr after switch
		std::vector<boundary_condition<T>> *dir_bcs = nullptr;

		int cell_start, cell_end;

		// clamp to 0 - ncols/norws
		switch (bc.edge) {
		case (boundary::edge_t::N):
			dir_bcs = &bc_north();
			cell_start =
			    ::util::clamp(static_cast<int>((bc.start - header.xllcorner) / header.cellsize), 0,
			                  header.ncols - 1);
			cell_end =
			    ::util::clamp(static_cast<int>((bc.end - header.xllcorner) / header.cellsize), 0,
			                  header.ncols - 1);
			break;
		case (boundary::edge_t::E):
			dir_bcs = &bc_east();
			cell_start =
			    ::util::clamp(static_cast<int>((bc.start - header.yllcorner) / header.cellsize), 0,
			                  header.nrows - 1);
			cell_end =
			    ::util::clamp(static_cast<int>((bc.end - header.yllcorner) / header.cellsize), 0,
			                  header.nrows - 1);
			break;
		case (boundary::edge_t::S):
			dir_bcs = &bc_south();
			cell_start =
			    ::util::clamp(static_cast<int>((bc.start - header.xllcorner) / header.cellsize), 0,
			                  header.ncols - 1);
			cell_end =
			    ::util::clamp(static_cast<int>((bc.end - header.xllcorner) / header.cellsize), 0,
			                  header.ncols - 1);
			break;
		case (boundary::edge_t::W):
			dir_bcs = &bc_west();
			cell_start =
			    ::util::clamp(static_cast<int>((bc.start - header.yllcorner) / header.cellsize), 0,
			                  header.nrows - 1);
			cell_end =
			    ::util::clamp(static_cast<int>((bc.end - header.yllcorner) / header.cellsize), 0,
			                  header.nrows - 1);
			break;
		default:
			spdlog::get("console")->error("Couldn't discern boundary condition edge type");
			return false;
		}

		// assign bcs and check for overlaps
		for (int i = cell_start; i < cell_end; ++i) {
			if (dir_bcs->at(i).type == boundary::type_t::closed)
				dir_bcs->at(i) = bc;
			else {
				spdlog::get("console")->error(
				    "Overlapping boundary conditions at Boundary condition #" +
				    std::to_string(bc_counter));
				return false;
			}
		}

		// if type of current bc is discharge also check if on dry cell and calculate flowDistr
		if (bc.type == boundary::type_t::discharge) {

			auto depth_north = state.depth.get_row(0);
			auto depth_east = state.depth.get_column(0);
			auto depth_south = state.depth.get_row(header.nrows - 1);
			auto depth_west = state.depth.get_column(header.ncols - 1);

			std::vector<T> *flow_dist_north = &state.flow_dist_north();
			std::vector<T> *flow_dist_east = &state.flow_dist_east();
			std::vector<T> *flow_dist_south = &state.flow_dist_south();
			std::vector<T> *flow_dist_west = &state.flow_dist_west();

			// calculate sum of depths along edge
			T sum_depth{0};
			for (int i = cell_start; i < cell_end; ++i) {
				if (dir_bcs->at(i).type == boundary::type_t::discharge) {
					std::vector<T> entries;

					switch (bc.edge) {
					case (boundary::edge_t::N):
						entries = depth_north;
						break;
					case (boundary::edge_t::E):
						entries = depth_east;
						break;
					case (boundary::edge_t::S):
						entries = depth_south;
						break;
					case (boundary::edge_t::W):
						entries = depth_west;
						break;
					default:
						spdlog::get("console")->error(
						    "Couldn't discern boundary condition edge type");
						return false;
					}
					sum_depth += entries.at(i);
				}
			}

			// calculate flow distribution
			for (int i = cell_start; i < cell_end; ++i) {
				if (dir_bcs->at(i).type == boundary::type_t::discharge) {
					std::vector<T> entries;
					std::vector<T> *flow_dist = nullptr;

					switch (bc.edge) {
					case (boundary::edge_t::N):
						entries = depth_north;
						flow_dist = flow_dist_north;
						break;
					case (boundary::edge_t::E):
						entries = depth_east;
						flow_dist = flow_dist_east;
						break;
					case (boundary::edge_t::S):
						entries = depth_south;
						flow_dist = flow_dist_south;
						break;
					case (boundary::edge_t::W):
						entries = depth_west;
						flow_dist = flow_dist_west;
						break;
					default:
						spdlog::get("console")->error(
						    "Couldn't discern boundary condition edge type");
						return false;
					}

					T depth = entries.at(i);
					if (depth <= T(DRY)) {
						spdlog::get("console")->error(
						    "Trying to impose flow boundary condition on a dry "
						    "boundary cell at boundary condition #" +
						    std::to_string(bc_counter));
						return false;
					}
					flow_dist->at(i) = depth / sum_depth;
				}
			}
		}

		// print information about boundary_conditions
		std::string axis = "";
		if (bc.edge == boundary::edge_t::N || bc.edge == boundary::edge_t::S)
			axis = "x";
		else if (bc.edge == boundary::edge_t::E || bc.edge == boundary::edge_t::W)
			axis = "y";
		switch (bc.type) {
		case (boundary::type_t::eta):
			spdlog::get("console")->info("water level [m] BC on " + edge_to_str(bc.edge) +
			                             " edge, from " + axis + " = " + std::to_string(bc.start) +
			                             " m to " + axis + " = " + std::to_string(bc.end) + " m");
			break;

		case (boundary::type_t::discharge):
			spdlog::get("console")->info("flow[m3 / s] BC on " + edge_to_str(bc.edge) +
			                             " edge, from " + axis + " = " + std::to_string(bc.start) +
			                             " m to " + axis + " = " + std::to_string(bc.end) + " m");
			break;
		case (boundary::type_t::open):
			spdlog::get("console")->info("open BC on " + edge_to_str(bc.edge) + " edge, from " +
			                             axis + " = " + std::to_string(bc.start) + " m to " + axis +
			                             " = " + std::to_string(bc.end) + " m");
			break;
		case (boundary::type_t::closed):
			// don't print anything on closed
			break;
		default:
			spdlog::get("console")->error("Couldn't discern boundary condition type");
			return false;
		}

		++bc_counter;
	}

	return true;
}

template <typename T>
std::vector<boundary::boundary_condition<T>> &
hn::boundary::boundary_condition_accessor<T>::bc_north()
{
	return boundary_conditions[0];
}

template <typename T>
std::vector<boundary::boundary_condition<T>> &boundary::boundary_condition_accessor<T>::bc_east()
{
	return boundary_conditions[1];
}

template <typename T>
std::vector<boundary::boundary_condition<T>> &boundary::boundary_condition_accessor<T>::bc_south()
{
	return boundary_conditions[2];
}

template <typename T>
std::vector<boundary::boundary_condition<T>> &boundary::boundary_condition_accessor<T>::bc_west()
{
	return boundary_conditions[3];
}
}
