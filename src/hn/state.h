#pragma once

#include <array>
#include <map>

#include "util/ghost_array.h"
#include "hn/config.h"
#include "io/esri_header.h"

namespace hn {
// forward decl
template <typename T> class config;

template <typename T> class state {
  public:
	::util::ghost_array<T> height{};     // eta = total height
	::util::ghost_array<T> depth{};      // h = eta - dem
	::util::ghost_array<T> x_velocity{}; // u
	::util::ghost_array<T> y_velocity{}; // v
	::util::ghost_array<T> x_flow{};     // qx
	::util::ghost_array<T> y_flow{};     // qy

	std::array<std::vector<T>, 4> flow_distributions{}; // N E S W

	std::vector<T> &flow_dist_north();
	std::vector<T> &flow_dist_east();
	std::vector<T> &flow_dist_south();
	std::vector<T> &flow_dist_west();

	bool write(const typename ::io::esri_header<T> &header, const config<T> &config, T time,
	           const ::util::ghost_array<T> &dem, bool dump = false);

	// reset all values, fill arrays with 0
	void clear();
};
}
