#pragma once

#include "hn/reconstruction.h"

namespace hn {

template <typename T> class wetdry {
  public:
	// name scheme: e.g. etaLeWD -> etaLefteast for
	// Left, Right, Up, Down, east, west, north, south
	T etaLeWD{}, etaReWD{}, etaLwWD{}, etaRwWD{}, etaDnWD{}, etaUnWD{}, etaDsWD{}, etaUsWD{};
	T hLeWD{}, hReWD{}, hLwWD{}, hRwWD{}, hDnWD{}, hUnWD{}, hDsWD{}, hUsWD{};
	T qxLeWD{}, qxReWD{}, qxLwWD{}, qxRwWD{}, qxDnWD{}, qxUnWD{}, qxDsWD{}, qxUsWD{};
	T qyLeWD{}, qyReWD{}, qyLwWD{}, qyRwWD{}, qyDnWD{}, qyUnWD{}, qyDsWD{}, qyUsWD{};
	T zbe{}, zbw{}, zbn{}, zbs{};

	void wet_dry(const reconstruction<T> &recon);

  private:
	void wet_dry_intern(const T &etaL, const T &etaR, T &etaLWD, T &etaRWD, T &hLWD, T &hRWD,
	                    T &qxLWD, T &qxRWD, T &qyLWD, T &qyRWD, const T &uL, const T &uR,
	                    const T &vL, const T &vR, const T &zbL, const T &zbR, T &zbface);
};

template <typename T> class wetdry_parallel {
  public:
	wetdry_parallel(int ncols, int nrows);
	wetdry_parallel() = default;

	// name scheme: e.g. etaLeWD -> etaLefteast for
	// Left, Right, Up, Down, east, west, north, south
	util::ghost_array<T> etaLeWD{}, etaReWD{}, etaLwWD{}, etaRwWD{}, etaDnWD{}, etaUnWD{},
	    etaDsWD{}, etaUsWD{};
	util::ghost_array<T> hLeWD{}, hReWD{}, hLwWD{}, hRwWD{}, hDnWD{}, hUnWD{}, hDsWD{}, hUsWD{};
	util::ghost_array<T> qxLeWD{}, qxReWD{}, qxLwWD{}, qxRwWD{}, qxDnWD{}, qxUnWD{}, qxDsWD{},
	    qxUsWD{};
	util::ghost_array<T> qyLeWD{}, qyReWD{}, qyLwWD{}, qyRwWD{}, qyDnWD{}, qyUnWD{}, qyDsWD{},
	    qyUsWD{};
	util::ghost_array<T> zbe{}, zbw{}, zbn{}, zbs{};

	void inline wet_dry_east(const reconstruction_parallel<T> &recon, const int x, const int y);
	void wet_dry_west(const reconstruction_parallel<T> &recon, const int x, const int y);
	void wet_dry_north(const reconstruction_parallel<T> &recon, const int x, const int y);
	void wet_dry_south(const reconstruction_parallel<T> &recon, const int x, const int y);

  private:
	void wet_dry_intern(const util::ghost_array<T> &etaL, const util::ghost_array<T> &etaR,
	                    util::ghost_array<T> &etaLWD, util::ghost_array<T> &etaRWD,
	                    util::ghost_array<T> &hLWD, util::ghost_array<T> &hRWD,
	                    util::ghost_array<T> &qxLWD, util::ghost_array<T> &qxRWD,
	                    util::ghost_array<T> &qyLWD, util::ghost_array<T> &qyRWD,
	                    const util::ghost_array<T> &uL, const util::ghost_array<T> &uR,
	                    const util::ghost_array<T> &vL, const util::ghost_array<T> &vR,
	                    const util::ghost_array<T> &zbL, const util::ghost_array<T> &zbR,
	                    util::ghost_array<T> &zbface, const int x, const int y);
};
} // namespace hn
