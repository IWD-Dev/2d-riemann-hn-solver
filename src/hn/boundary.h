#pragma once

#include <vector>
#include <array>

#include "hn/boundary_types.h"
#include "hn/hydrograph.h"
#include "hn/state.h"
#include "io/esri_header.h"
#include "util/ghost_array.h"

namespace hn {
// forward decl
template <typename T> class config;
template <typename T> class state;

namespace boundary {
	// flags for identifying boundary numbers along ghost cells
	// HydroGraph ID

	// flags for identifying boundary types along ghost cells
	// eta bc -> bcnE = hydrgCount, fube = 1, fetae = 1
	// Q bc -> bcnE = hydrgCount, fube = 1, fqe = 1
	// open bc -> bcnE = hydrgCount, fope = 1

	// factors for flow distribution along boundaries
	//

	edge_t str_to_edge(const std::string &str);
	std::string edge_to_str(const edge_t &edge);

	type_t str_to_type(const std::string &str);
	std::string type_to_str(const type_t &type);

	template <typename T> class boundary_condition {
	  public:
		// edge this bc is set on
		edge_t edge{};

		// positional start
		T start{};

		// positional end
		T end{};

		// type of outgoing bc
		type_t type{};

		// name of hydrograph this has, "" (empty string) if none
		std::string hydrograph{};
	};

	template <typename T> class boundary_condition_accessor {
	  public:
		// could do this with a map(pair(col,row), boundary_identifier) instead
		std::array<std::vector<boundary_condition<T>>, 4> boundary_conditions; // N E S W

		bool initialize(const config<T> &config, const typename io::esri_header<T> header,
		                state<T> &state);

		std::vector<boundary_condition<T>> &bc_north();
		std::vector<boundary_condition<T>> &bc_east();
		std::vector<boundary_condition<T>> &bc_south();
		std::vector<boundary_condition<T>> &bc_west();
	};
}
}
