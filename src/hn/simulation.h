#pragma once

#include "hn/config.h"
#include "hn/boundary.h"

#include "util/ghost_array.h"
#include "io/esri.h"
#include "hn/state.h"

namespace hn {
template <typename T> class simulation {
  public:
	::util::ghost_array<T> dem{};
	::util::ghost_array<T> manning{};

	// for easier boundary management, rather then manually accessing config
	// basically a lookup structure
	boundary::boundary_condition_accessor<T> bc_accessor;

	// states holding simulation time-dependent data
	state<T> curr{};
	state<T> read{};
	state<T> write{};

	simulation();
	virtual ~simulation();

	// functions
	bool setup(const std::string &config_path);
	bool run_sequential();
	bool run_parallel();

	// getter
	config<T> get_config() { return config; };
	typename ::io::esri_header<T> get_header() { return header; };

  private:
	// config & header are private to assure read-only-ness
	config<T> config;
	typename ::io::esri_header<T> header{};
};
}
