#pragma once

#include "util/ghost_array.h"

namespace hn {

template <typename T> class reconstruction {
  public:
	enum class dtype { eta, h, qx, qy };

	// name scheme: e.g. etaLe -> etaLefteast for
	// Left, Right, Up, Down, east, west, north, south
	T etaLe{}, etaRe{}, etaLw{}, etaRw{}, etaDn{}, etaUn{}, etaDs{}, etaUs{};
	T hLe{}, hRe{}, hLw{}, hRw{}, hDn{}, hUn{}, hDs{}, hUs{};
	T uLe{}, uRe{}, uLw{}, uRw{}, uDn{}, uUn{}, uDs{}, uUs{};
	T vLe{}, vRe{}, vLw{}, vRw{}, vDn{}, vUn{}, vDs{}, vUs{};
	T qxLe{}, qxRe{}, qxLw{}, qxRw{}, qxDn{}, qxUn{}, qxDs{}, qxUs{};
	T qyLe{}, qyRe{}, qyLw{}, qyRw{}, qyDn{}, qyUn{}, qyDs{}, qyUs{};
	T zbLe{}, zbRe{}, zbLw{}, zbRw{}, zbDn{}, zbUn{}, zbDs{}, zbUs{};

	void fill_1dry(const util::ghost_array<T> &eta, const util::ghost_array<T> &h,
	               const util::ghost_array<T> &u, const util::ghost_array<T> &v,
	               const util::ghost_array<T> &qx, const util::ghost_array<T> &qy,
	               const util::ghost_array<T> &zb, const int x, const int y);

	void fill_1drywet(const util::ghost_array<T> &eta, const util::ghost_array<T> &h,
	                  const util::ghost_array<T> &u, const util::ghost_array<T> &v,
	                  const util::ghost_array<T> &qx, const util::ghost_array<T> &qy,
	                  const util::ghost_array<T> &zb, const int x, const int y);

	void fill_2inn(const dtype type, const util::ghost_array<T> &arr, const T dx, const T dy,
	               const int x, const int y);

	void fill_2out(const dtype type, const util::ghost_array<T> &arr, const T dx, const T dy,
	               const int x, const int y, const int ncols, const int nrows);

	// recalculates zb, u, v
	void recalculate();

	inline T minmod(T val1, T val2);
};

template <typename T> class reconstruction_parallel {
  public:
	enum class dtype { eta, h, qx, qy };

	reconstruction_parallel(int ncols, int nrows);
	reconstruction_parallel() = default;

	// name scheme: e.g. etaLe -> etaLefteast for
	// Left, Right, Up, Down, east, west, north, south
	util::ghost_array<T> etaLe, etaRe, etaLw, etaRw, etaDn, etaUn, etaDs, etaUs;
	util::ghost_array<T> hLe, hRe, hLw, hRw, hDn, hUn, hDs, hUs;
	util::ghost_array<T> uLe, uRe, uLw, uRw, uDn, uUn, uDs, uUs;
	util::ghost_array<T> vLe, vRe, vLw, vRw, vDn, vUn, vDs, vUs;
	util::ghost_array<T> qxLe, qxRe, qxLw, qxRw, qxDn, qxUn, qxDs, qxUs;
	util::ghost_array<T> qyLe, qyRe, qyLw, qyRw, qyDn, qyUn, qyDs, qyUs;
	util::ghost_array<T> zbLe, zbRe, zbLw, zbRw, zbDn, zbUn, zbDs, zbUs{};

	void fill_1dry_eta(const util::ghost_array<T> &eta, const int x, const int y);
	void fill_1dry_h(const util::ghost_array<T> &h, const int x, const int y);
	void fill_1dry_u(const util::ghost_array<T> &u, const int x, const int y);
	void fill_1dry_v(const util::ghost_array<T> &v, const int x, const int y);
	void fill_1dry_qx(const util::ghost_array<T> &qx, const int x, const int y);
	void fill_1dry_qy(const util::ghost_array<T> &qy, const int x, const int y);
	void fill_1dry_zb(const util::ghost_array<T> &zb, const int x, const int y);

	void fill_1drywet_eta(const util::ghost_array<T> &eta, const int x, const int y);
	void fill_1drywet_h(const util::ghost_array<T> &h, const int x, const int y);
	void fill_1drywet_u(const util::ghost_array<T> &u, const int x, const int y);
	void fill_1drywet_v(const util::ghost_array<T> &v, const int x, const int y);
	void fill_1drywet_qx(const util::ghost_array<T> &qx, const int x, const int y);
	void fill_1drywet_qy(const util::ghost_array<T> &qy, const int x, const int y);
	void fill_1drywet_zb(const util::ghost_array<T> &zb, const int x, const int y);

	void fill_2inn_eta(const util::ghost_array<T> &eta, const T dx, const T dy, const int x,
	                   const int y);
	void fill_2inn_h(const util::ghost_array<T> &h, const T dx, const T dy, const int x,
	                 const int y);
	void fill_2inn_qx(const util::ghost_array<T> &qx, const T dx, const T dy, const int x,
	                  const int y);
	void fill_2inn_qy(const util::ghost_array<T> &qy, const T dx, const T dy, const int x,
	                  const int y);

	void fill_2out_eta(const util::ghost_array<T> &eta, const T dx, const T dy, const int x,
	                   const int y, const int ncols, const int nrows);
	void fill_2out_h(const util::ghost_array<T> &h, const T dx, const T dy, const int x,
	                 const int y, const int ncols, const int nrows);
	void fill_2out_qx(const util::ghost_array<T> &qx, const T dx, const T dy, const int x,
	                  const int y, const int ncols, const int nrows);
	void fill_2out_qy(const util::ghost_array<T> &qy, const T dx, const T dy, const int x,
	                  const int y, const int ncols, const int nrows);

	// TODO: these might be need to split into 8 functions each
	void recalculate_u(const int x, const int y);
	void recalculate_v(const int x, const int y);
	void recalculate_zb(const int x, const int y);

	inline T minmod(T val1, T val2);
};
}
