#pragma once

#include <string>

namespace util {
template <typename T> inline T clamp(const T &n, const T &min, const T &max)
{
	return std::max(min, std::min(n, max));
}

template <typename T> inline bool nearly_equal(T a, T b, T threshold = static_cast<T>(0.00001))
{
	return fabs(a - b) < threshold;
}
// a nearly bigger than b
template <typename T>
inline bool nearly_greater_equal(T a, T b, T threshold = static_cast<T>(0.00001))
{
	return (a + threshold) >= b;
}

template <typename T> inline T sqr(T val) { return val * val; }

// to_numerical spezialized for some types
template <typename T> T to_numerical(const std::string &str) { return static_cast<T>(-1); }
template <> int to_numerical<int>(const std::string &str);
template <> float to_numerical<float>(const std::string &str);
template <> double to_numerical<double>(const std::string &str);

std::vector<std::string> split_string(const std::string &text, char sep, size_t size_hint = 3);

std::string get_right_of_delim(std::string const &str, std::string const &delim);
}
