#pragma once

#define FMT_HEADER_ONLY
#define FMT_USE_WINDOWS_H 0 // don't need windows system logging
#include "extern/spdlog/spdlog.h"

namespace util {
namespace log {
	void setup()
	{
		spdlog::set_level(spdlog::level::info);
		spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%l] %v");

		auto log = spdlog::stdout_color_mt("console");
	}
}
}