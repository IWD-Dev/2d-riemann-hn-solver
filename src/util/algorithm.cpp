#include "stdafx.h"

#include "util/algorithm.h"

namespace util {

template <> int to_numerical<int>(const std::string &str) { return std::stoi(str); }
template <> float to_numerical<float>(const std::string &str) { return std::stof(str); }
template <> double to_numerical<double>(const std::string &str) { return std::stod(str); }

std::vector<std::string> split_string(const std::string &text, char sep, size_t size_hint)
{
	std::vector<std::string> tokens;
	tokens.reserve(size_hint);
	std::size_t start = 0, end = 0;
	while ((end = text.find(sep, start)) != std::string::npos) {
		tokens.push_back(text.substr(start, end - start));
		start = end + 1;
	}
	tokens.push_back(text.substr(start));
	return tokens;
}

std::string get_right_of_delim(std::string const &str, std::string const &delim)
{
	return str.substr(str.find(delim) + delim.size());
}
}