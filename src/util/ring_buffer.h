#pragma once

#include <vector>

namespace util {

template <typename T> class ring_buffer {
  public:
	ring_buffer(size_t size) : size(size), pos(0ull), filled_once(false) { data.reserve(size); };
	/*virtual*/ ~ring_buffer(){/* intentionally empty*/};

	T &at(size_t pos) { return data.at(pos); }
	void push(T entry)
	{
		if (filled_once) {
			data.at(pos) = entry;
		}
		else {
			data.push_back(entry);
		}

		pos = (pos + 1ull) % size;

		if (!filled_once && pos == 0) {
			filled_once = true;
		}
	}

	typename std::vector<T>::iterator raw_begin() { return data.begin(); }
	typename std::vector<T>::iterator raw_end() { return data.end(); }

	std::vector<T> get() { return data; }

  private:
	std::vector<T> data;
	size_t size;
	size_t pos;

	bool filled_once;
};

} // namespace util
