#pragma once

#include <vector>
#include <string>
#include <sstream>

#include "util/algorithm.h"

namespace util {

// clamp:	use nearest value
enum class ghost_array_boundary_wrap { clamp };

template <typename T> class ghost_array {

	// TODO: maybe implement as random access iterator
	// https://stackoverflow.com/questions/3582608/how-to-correctly-implement-custom-iterators-and-const-iterators
	template <typename U> class gost_array_iterator;

  public:
	template <typename U> class ghost_array_iterator {
		friend class ghost_array<T>;

	  public:
		// define this as std::iterator is deprecated in c++17
		using iterator_category = std::forward_iterator_tag;
		using value_type = U;
		using difference_type = ptrdiff_t;
		using pointer = U *;
		using reference = U &;

		ghost_array_iterator<U>(ghost_array *parent) : parent(parent), col(0), row(0) {}

		ghost_array_iterator<U>(const ghost_array_iterator<T> &other)
		{
			curr = other.curr;
			col = other.col;
			row = other.row;
			parent = other.parent;
		}

		ghost_array_iterator<U> &operator=(const ghost_array_iterator<U> &other)
		{
			curr = other.curr;
			col = other.col;
			row = other.row;
			parent = other.parent;
			return *this;
		}

		ghost_array_iterator<U> &operator++() // pre
		{
			++col;
			if (col % parent->ncols() == 0) {
				++row;
				if (row < parent->nrows()) {
					col = 0;
					curr += 3;
				}
				else
					curr = parent->end().get_curr();
			}
			else
				++curr;

			return *this;
		}

		ghost_array_iterator<U> operator++(int) // post
		{
			ghost_array_iterator<U> ret(*this);
			++(*this);
			return ret;
		}

		T *operator->() const { return &(operator*()); }

		bool operator==(const ghost_array_iterator<U> &other) const
		{
			// TODO: also check col & row
			return curr == other.curr;
		}

		bool operator!=(const ghost_array_iterator<U> &other) const { return !(*this == other); }

		U &operator*() { return *curr; }

		typename std::vector<T>::iterator get_curr() const { return curr; }
		int get_col() const { return col; }
		int get_row() const { return row; }

	  private:
		typename std::vector<T>::iterator curr;
		int col;
		int row;
		ghost_array *parent;
	};

	//---------------------------------------------------------------//

	ghost_array_iterator<T> begin()
	{
		ghost_array_iterator<T> ret(this);
		ret.curr = data.begin() + cols + 3;
		return ret;
	}

	ghost_array_iterator<T> end()
	{
		ghost_array_iterator<T> ret(this);
		ret.curr = data.begin() + (rows) * (cols + 2) + cols + 1;
		return ret;
	}

	ghost_array(int ncols = 1, int nrows = 1)
	{
		assert(ncols != 0 && nrows != 0);
		// in release we should throw an exception
		if (ncols == 0 || nrows == 0)
			throw std::domain_error("ncols and nrows musn't be 0; ncols: " + std::to_string(ncols) +
			                        " nrows: " + std::to_string(nrows));

		this->cols = ncols;
		this->rows = nrows;
		this->data.resize((rows + 2) * (cols + 2));
	}

	/*virtual*/ ~ghost_array(){/* intentionally empty*/};

	std::vector<T> get_data(bool with_ghost = false)
	{
		if (with_ghost)
			return data;
		else {
			std::vector<T> new_data{};
			new_data.reserve(cols * rows);
			for (int y = 0; y < rows; ++y) {
				for (int x = 0; x < cols; ++x) {
					new_data.push_back(this->at(x, y));
				}
			}
			return new_data;
		}
	}

	std::vector<T> get_data(bool with_ghost = false) const
	{
		if (with_ghost)
			return data;
		else {
			std::vector<T> new_data{};
			new_data.reserve(cols * rows);
			for (int y = 0; y < rows; ++y) {
				for (int x = 0; x < cols; ++x) {
					new_data.push_back(this->at(x, y));
				}
			}
			return new_data;
		}
	}

	// direct access, can also access ghost cells
	T &at(int n) { return data[n]; };
	const T &at(int n) const { return data[n]; };

	// can only access direct values, ghost cells with e.g. -1 or +1
	// safe access (boundary check: clamp)
	T &at(int col, int row)
	{
		int s_col = util::clamp(col, -1, cols);
		int s_row = util::clamp(row, -1, rows);
		return data[((s_row + 1) * (cols + 2) + 1) + s_col];
	}
	const T &at(int col, int row) const
	{
		int s_col = util::clamp(col, -1, cols);
		int s_row = util::clamp(row, -1, rows);
		return data[((s_row + 1) * (cols + 2) + 1) + s_col];
	}

	// fast at, no range check!
	// if unsure that col and row are in bounds use "normal" at
	T &f_at(int col, int row) { return data[((row + 1) * (cols + 2) + 1) + col]; }
	const T &f_at(int col, int row) const { return data[((row + 1) * (cols + 2) + 1) + col]; }

	// calls f_at, no range check!
	inline T &operator()(int col, int row) { return f_at(col, row); };
	inline const T &operator()(int col, int row) const { return f_at(col, row); };

	std::vector<T> get_column(int column)
	{
		assert(column >= 0 && column < cols);

		std::vector<T> new_column{};
		new_column.reserve(rows);
		for (int row = 0; row < rows; ++row) {
			new_column.push_back(this->at(column, row));
		}
		return new_column;
	}

	std::vector<T> get_column(int column) const
	{
		assert(column >= 0 && column < cols);

		std::vector<T> new_column{};
		new_column.reserve(rows);
		for (int row = 0; row < rows; ++row) {
			new_column.push_back(this->at(column, row));
		}
		return new_column;
	}

	std::vector<T> get_row(int row)
	{
		assert(row >= 0 && row < cols);

		std::vector<T> new_row{};
		new_row.reserve(cols);
		for (int col = 0; col < cols; ++col) {
			new_row.push_back(this->at(col, row));
		}
		return new_row;
	}

	std::vector<T> get_row(int row) const
	{
		assert(row >= 0 && row < cols);

		std::vector<T> new_row{};
		new_row.reserve(cols);
		for (int col = 0; col < cols; ++col) {
			new_row.push_back(this->at(col, row));
		}
		return new_row;
	}

	int ncols() { return cols; }
	int ncols() const { return cols; }
	int nrows() { return rows; }
	int nrows() const { return rows; }

	void fill(T value) { std::fill(std::begin(data), std::end(data), value); }

	void fill_ghost_cells(ghost_array_boundary_wrap wrap)
	{
		switch (wrap) {
		case ghost_array_boundary_wrap::clamp:
			// TOP
			for (int col = 1; col < cols + 1; ++col) {
				data[col] = data[cols + 2 + col];
			}
			// BOTTOM
			for (int col = 1; col < cols + 1; ++col) {
				data[(rows + 1) * (cols + 2) + col] = data[rows * (cols + 2) + col];
			}
			// LEFT
			for (int row = 1; row < rows + 1; ++row) {
				data[row * (cols + 2)] = data[row * (cols + 2) + 1];
			}
			// RIGHT
			for (int row = 1; row < rows + 1; ++row) {
				data[row * (cols + 2) + cols + 1] = data[row * (cols + 2) + cols];
			}
			// CORNERS
			data[0] = data[cols + 3];                                                    // TL
			data[cols + 1] = data[(cols + 2) + cols];                                    // TR
			data[(rows + 1) * (cols + 2)] = data[(rows) * (cols + 2) + 1];               // BL
			data[(rows + 1) * (cols + 2) + cols + 1] = data[(rows) * (cols + 2) + cols]; // BR
			break;
		default:
			break;
		}
	}

	std::string to_string(bool with_ghost = false)
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					str += std::to_string(at(col, row)) + "\t";
				}
				str += std::to_string(at(cols, row)) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					str += std::to_string(data[row * (cols + 2) + col]) + "\t";
				}
				str += std::to_string(data[row * (cols + 2) + cols + 1]) + "\n";
			}
		}
		return str;
	}
	std::string to_string(bool with_ghost = false) const
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					str += std::to_string(at(col, row)) + "\t";
				}
				str += std::to_string(at(cols, row)) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					str += std::to_string(data[row * (cols + 2) + col]) + "\t";
				}
				str += std::to_string(data[row * (cols + 2) + cols + 1]) + "\n";
			}
		}
		return str;
	}

	std::string to_string_constrained(T min, T max, bool with_ghost = false)
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					str += std::to_string(::util::clamp(at(col, row), min, max)) + "\t";
				}
				str += std::to_string(::util::clamp(at(cols, row), min, max)) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					str += std::to_string(::util::clamp(data[row * (cols + 2) + col], min, max)) +
					       "\t";
				}
				str += std::to_string(::util::clamp(data[row * (cols + 2) + cols + 1], min, max)) +
				       "\n";
			}
		}
		return str;
	}
	std::string to_string_constrained(T min, T max, bool with_ghost = false) const
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					str += std::to_string(::util::clamp(at(col, row), min, max)) + "\t";
				}
				str += std::to_string(::util::clamp(at(cols, row), min, max)) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					str += std::to_string(::util::clamp(data[row * (cols + 2) + col], min, max)) +
					       "\t";
				}
				str += std::to_string(::util::clamp(data[row * (cols + 2) + cols + 1], min, max)) +
				       "\n";
			}
		}
		return str;
	}

	std::string to_string_threshold(T thresh, T set_value, bool with_ghost = false)
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					auto val = at(col, row);
					val = val <= thresh ? set_value : val;
					str += std::to_string(val) + "\t";
				}
				auto val = at(cols, row);
				val = val <= thresh ? set_value : val;
				str += std::to_string(val) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					auto val = data[row * (cols + 2) + col];
					val = val <= thresh ? set_value : val;
					str += std::to_string(val) + "\t";
				}
				auto val = data[row * (cols + 2) + cols + 1];
				val = val <= thresh ? set_value : val;
				str += std::to_string(val) + "\n";
			}
		}
		return str;
	}
	std::string to_string_threshold(T thresh, T set_value, bool with_ghost = false) const
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					auto val = at(col, row);
					val = val <= thresh ? set_value : val;
					str += std::to_string(val) + "\t";
				}
				auto val = at(cols, row);
				val = val <= thresh ? set_value : val;
				str += std::to_string(val) + "\n";
			}
		}
		else {
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					auto val = data[row * (cols + 2) + col];
					val = val <= thresh ? set_value : val;
					str += std::to_string(val) + "\t";
				}
				auto val = data[row * (cols + 2) + cols + 1];
				val = val <= thresh ? set_value : val;
				str += std::to_string(val) + "\n";
			}
		}
		return str;
	}

	std::string to_string_threshold_array(T thresh, ghost_array<T> other, bool with_ghost = false)
	{
		std::string str{};

		if (!with_ghost) {
			for (int row = 0; row < rows; ++row) {
				for (int col = 0; col < cols - 1; ++col) {
					auto val = at(col, row);
					val = val <= thresh ? other.at(col, row) : val;
					str += std::to_string(val) + "\t";
				}
				auto val = at(cols, row);
				val = val <= thresh ? other.at(cols, row) : val;
				str += std::to_string(val) + "\n";
			}
		}
		else {
			auto other_data = other.get_data(true);
			for (int row = 0; row < rows + 2; ++row) {
				for (int col = 0; col < cols + 1; ++col) {
					auto val = data[row * (cols + 2) + col];
					val = val <= thresh ? other_data[row * (cols + 2) + col] : val;
					str += std::to_string(val) + "\t";
				}
				auto val = data[row * (cols + 2) + cols + 1];
				val = val <= thresh ? other_data[row * (cols + 2) + cols + 1] : val;
				str += std::to_string(val) + "\n";
			}
		}
		return str;
	}

  private:
	int cols;
	int rows;
	std::vector<T> data;
};
} // namespace util
