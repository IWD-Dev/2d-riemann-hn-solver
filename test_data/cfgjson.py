import json

dic = {}
dic["time_stop"] = 600
dic["time_step_AIMD_penalty"] = 0.5
dic["time_step_AIMD_bonus"] = 2.0
dic["time_step_AIMD_increase"] = 0.00002
dic["time_step_min"] = 0.0
dic["time_step_max"] = 999.999
dic["time_sim_out_file"] = 2.0
dic["time_sim_console_log"] = 1.0 # every second
dic["courant_tolerance_high"] = 1.0
dic["courant_tolerance_low"] = 0.1

hydrographs = [{
    "name" : "eta1",
    "steps" : [
        {"time": 0, "value": 3.65},
        {"time": 500, "value": 5.50},
        {"time": 3600, "value": 5.50}
        ]
},
{
    "name" : "eta2",
    "steps" : [
        {"time": 0, "value": 0.5},
        {"time": 3600, "value": 0.5}
        ]
}]

bc = [{
    "edge": "E",
    "start": 10.70117,
    "end": 95.95117,
    "type": "open",
    "hydrograph": ""
    },
    {
    "edge": "W",
    "start": 10.70117,
    "end": 95.95117,
    "type": "open",
    "hydrograph": ""
    },
    {
    "edge": "N",
    "start": 45.10408,
    "end": 135.1041,
    "type": "open",
    "hydrograph": ""
    },
    {
    "edge": "S",
    "start": 45.104,
    "end": 100.23,
    "type": "eta",
    "hydrograph": "eta1"
    }]

cc = [{
    "x_start": 20.0,
    "x_end": 21.0,
    "y_start": 20.0,
    "y_end": 21.0,
    "type": "eta",
    "hydrograph": "eta2"
    }]

dic["hydrographs"] = hydrographs
dic["boundary_conditions"] = bc
dic["cell_conditions"] = cc

dic["in_dem"] = "../test_data/fiktivdem.asc"
dic["in_eta"] = "../test_data/fiktivdem.asc"
dic["in_x_velocity"] = ""
dic["in_y_velocity"] = ""
dic["in_manning"] = "../test_data/fiktivman.asc"

dic["result_prefix"] = "res"
dic["out_path"] = "../test_data/res/fiktiv/"
dic["out_depth"] = False
dic["out_height"] = True
dic["out_x_velocity"] = True
dic["out_y_velocity"] = True
dic["out_vmag"] = False
dic["out_flowd"] = False

out = json.dumps(dic, sort_keys=True, indent=4, separators=(',', ': '))

with open('fiktiv.cfg', 'w') as outfile:
    outfile.write(out)